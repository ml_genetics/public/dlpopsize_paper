#!/usr/bin/python3

import argparse
import os

import numpy as np

import msprime
import pandas as pd
import json


def load_dict_from_json(filepath):
    """Load a json in a dictionnary.

    The dictionnary contains the overall parameters used for the simulation
    (e.g. path to the data folder, number of epoch). The json can be created
    from a dictionnary using ``save_dict_in_json``.

    Arguments:
        filepath (string): filepath to the json file
    """
    return json.loads(open(filepath).read())

def save_dict_in_json(filepath, param):
    """Save a dictionnary into a json file.

    Arguments:
        filepath (string): filepath of the json file
        param(dict): dictionnary containing the overall parameters used for
        the simulation (e.g. path to the data folder, number of epoch...)
    """
    with open(filepath, 'w') as file_handler:
        json.dump(param, file_handler)

def generate_scenarios_parameters(num_scenario, num_sample, tmax, mutation_rate,
                                  recombination_rate_min, recombination_rate_max,
                                  n_min, n_max, num_replicates, segment_length,
                                  num_time_windows, time_rate, **kwargs):
    """Generate demographic parameters.
    """
    scenario_param = pd.DataFrame(columns=['scenario', 'mutation_rate',
                                           'recombination_rate', 'num_replicates',
                                           'num_sample', 'segment_length'])
    scenario_param['scenario'] = np.array(range(num_scenario))
    scenario_param['mutation_rate'] = np.full(num_scenario, mutation_rate)
    scenario_param['recombination_rate'] = np.random.uniform(low=recombination_rate_min, high=recombination_rate_max, size=num_scenario)
    scenario_param['num_replicates'] = np.full(num_scenario, num_replicates)
    scenario_param['num_sample'] = np.full(num_scenario, num_sample)
    scenario_param['segment_length'] = np.full(num_scenario, segment_length)

    population_time = np.repeat([[(np.exp(np.log(1 + time_rate * tmax) * i /
                              (num_time_windows - 1)) - 1) / time_rate for i in
                              range(num_time_windows)]], num_scenario, axis=0)
    population_time = pd.DataFrame(np.around(population_time).astype(int), columns=[f'population_time_{i}' for i in range(num_time_windows)])
    n_min_log10 = np.log10(n_min)
    n_max_log10 = np.log10(n_max)
    population_size = [[10 ** np.random.uniform(low=n_min_log10, high=n_max_log10)] for i in range(num_scenario)]
    for i in range(num_scenario):
        for j in range(num_time_windows - 1):
            population_size[i].append(10 ** n_min_log10 - 1)
            while population_size[i][-1] > 10 ** n_max_log10 or population_size[i][-1]  < 10 ** n_min_log10:
                population_size[i][-1] = population_size[i][-2] * 10 ** np.random.uniform(-1, 1)

    population_size = pd.DataFrame(np.array(population_size).astype(int), columns=[f'population_size_{i}' for i in range(num_time_windows)])
    scenario_param = pd.concat([scenario_param, population_time, population_size], axis=1, sort=False)
    scenario_param = scenario_param.set_index('scenario')
    return scenario_param

def simulate_scenarios(scenario_param, num_time_windows, num_sample, model_name, num_scenario, seed_simulation, verbose=False,
                       data_path="", dump_tree=False, **kwargs):
    """Simulate scenarios from a pandas' DataFrame.
    """
    for scenario in scenario_param.index:
        os.makedirs(os.path.join(data_path, model_name, f'scenario_{scenario}'),
                    exist_ok=True)
        demographic_events = [msprime.PopulationParametersChange(
            time=scenario_param[f'population_time_{i}'][scenario],
            growth_rate=0,
            initial_size=scenario_param[f'population_size_{i}'][scenario]) for i in range(1, num_time_windows)]
        population_configurations = [msprime.PopulationConfiguration(
            sample_size=scenario_param['num_sample'][scenario],
            initial_size=scenario_param['population_size_0'][scenario])]
        if verbose:
            msprime.DemographyDebugger(
                population_configurations=population_configurations,
                demographic_events=demographic_events).print_history()
        tree_sequence = msprime.simulate(
            length=scenario_param['segment_length'][scenario],
            population_configurations=population_configurations,
            demographic_events=demographic_events,
            recombination_rate=scenario_param['recombination_rate'][scenario],
            mutation_rate=scenario_param['mutation_rate'][scenario],
            num_replicates=scenario_param['num_replicates'][scenario],
            random_seed=seed_simulation)
        for i, rep in enumerate(tree_sequence):
            if dump_tree:
                rep.dump(os.path.join(data_path, model_name,
                                      f'scenario_{scenario}',
                                      f'{model_name}_{scenario}_{i}_tree'))
            positions = [variant.site.position for variant in rep.variants()]
            positions = np.array(positions) - np.array([0] + positions[:-1])
            positions = positions.astype(int)
            SNPs = rep.genotype_matrix().T.astype(np.uint8)
            np.savez_compressed(os.path.join(data_path, model_name,
                                             f'scenario_{scenario}',
                                             f'{model_name}_{scenario}_{i}'),
                                SNP=SNPs, POS=positions)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--data_path", default="", type=str)
    parser.add_argument("--scenario_id", default=0, type=int)
    parser.add_argument("--nb_scenarios_to_simulate", default=None, type=int)
    parser.add_argument("--scenario_param_path", default=None, type=str)
    parser.add_argument("--simul_param_path", default=None, type=str)
    parser.add_argument("--verbose", default=False, type=bool)
    parser.add_argument("--simulate", default=False, type=bool)
    parser.add_argument("--dump_tree", default=False, type=bool)
    args = parser.parse_args()
    # Load or create simulation parameters
    if args.simul_param_path is None:
        simul_param = {'num_scenario': 2000, 'num_sample': 50, 'tmax': 130000,
                        'recombination_rate_min': 1e-9,
                        'recombination_rate_max': 1e-8, 'mutation_rate': 1e-8,
                        'n_min': 10, 'n_max': 100000, 'num_replicates': 100,
                        'segment_length': 2e6, 'model_name': 'cattle',
                        'data_path': args.data_path, 'seed_simulation': 2,
                        'num_time_windows': 21, 'time_rate': 0.06,
			'simulation_data_path': args.data_path}
        json_filename = simul_param['model_name'] + '_simul_parameters.json'
        os.makedirs(os.path.join(args.data_path, simul_param['model_name']),
                    exist_ok=True)
        save_dict_in_json(os.path.join(args.data_path,
                                       simul_param['model_name'],
                                       json_filename), simul_param)
    else:
        simul_param = load_dict_from_json(args.simul_param_path)
    # Set seed
    np.random.seed(simul_param['seed_simulation'])
    if args.scenario_param_path is None:
        # Generate a csv file with all demographic parameters
        scenario_param = generate_scenarios_parameters(**simul_param)
        os.makedirs(os.path.join(args.data_path, simul_param['model_name']),
                    exist_ok=True)
        scenario_param.to_csv(os.path.join(args.data_path,
                                            simul_param['model_name'],
                                            simul_param['model_name'] +
                                            '_demo_parameters.csv'),
                               index_label='scenario')
    else:
        skiprows = list(range(1, args.scenario_id + 1))
        scenario_param = pd.read_csv(args.scenario_param_path,
                                      index_col='scenario',
                                      skiprows=skiprows,
                                      nrows=args.nb_scenarios_to_simulate)
    # Perform the simulations
    if args.simulate:
        simulate_scenarios(scenario_param, **simul_param, verbose=args.verbose,
                           dump_tree=args.dump_tree)
