#!/usr/bin/env python
# coding: utf-8

# # DNA_DNA: Deep Neural Architecture for DNA
# This notebook reproduces an example of population size history inference performed by the SPIDNA deep learning method described in the paper ["Deep learning for population size history inference: design, comparison and combination with approximate Bayesian computation"](https://www.biorxiv.org/content/10.1101/2020.01.20.910539v1.full.pdf) (Sanchez et al.).
#
# We will simulate SNP data for six scenarios with population size history defined by hand (e.g. expansion, decline or bottleneck) and use a pretrained version of SPIDNA to reconstruct these population size histories. This architecture has been trained using data generated with msprime and prior described in Sanchez et al. [methods section](https://www.biorxiv.org/content/10.1101/2020.01.20.910539v1.full.pdf#page=9). Therefore, using the same architecture to infer population size histories far from this prior might lead to high prediction errors.

# ## Import and define functions

# In[1]:

import sys
sys.path.append("./simu/")

import numpy as np
import dl.net as net_module
import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim
import dl.utils as utils
import os
import msprime
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from collections import OrderedDict
import pickle
import simu.simu_utils as msu
import itertools
import importlib
import json
import random
from matplotlib.backends.backend_pdf import PdfPages
from pathlib import Path
import subprocess
from datetime import datetime
import argparse
import allel
from matplotlib.backends.backend_pdf import PdfPages


# In[2]:


a_time = datetime.now()
print(a_time)

parser = argparse.ArgumentParser(description='selection grid')
parser.add_argument('--task', dest='task_num', type=int,
                    help='Task number, used to label outputs')

parser.add_argument('--neutral', dest='neutral', type=str,
                    default='n', choices=['y','n'],
                    help='Neutral (y) or  Selection (n) [Default: Selection (n)]' )

parser.add_argument('--test', dest='run_test', type=str,
                    default='n',choices=['y','n'],
                    help='Test with small scale simulations (less replicates, fewer individuals, smaller regions etc) (y) or  Real Launch (n) [Default: Real launch (n)]')

parser.add_argument('--demo', dest='demo', type=str,
                    default='Medium',
                    help='Name of demographic scenario (eg Decline, Medium, Expansion, Large, Bottleneck, Zigzag).'
                    ' The scenario has to be defined in scenario_notebooks')

parser.add_argument('--nsam', dest='num_sample_vec', type=int, nargs='+',
                    default=[50],
                    help='List of sample sizes to be simulated')

parser.add_argument('--start_scenario', dest='start_scenario_num', type=int,
                    default=-1,
                    help='First scenario number used to index outputs')

parser.add_argument('--simulator', dest='simulator', type=str,
                    default='java -jar simu/msms3.2rc_b163.jar',
                    help='Simulator [Default: "java -jar simu/msms3.2rc_b163.jar"],'
                    ' choices: [msprime, ms, your preferred msms command "java -jar path_to_msms/msms/lib/msms.jar"]')

parser.add_argument('--overwrite', dest='overwrite', type=str,
                    default='n', choices=['y','n'],
                    help='Overwriting past scenarios ? (y or n)')


args = parser.parse_args()

task_num = args.task_num
#task_num = int(sys.argv[1]) #0
print(f'Task num {task_num}')

selection = args.neutral=='n'
print(f'With selection: {selection}')

run_test = args.run_test=='y'
print(f'Run test: {run_test}')

num_sample_vec = args.num_sample_vec
print("List of sample sizes to be simulated:")
print(num_sample_vec)


start_scenario_num = args.start_scenario_num

overwrite = args.overwrite=='y'
print(f'Overwriting: {overwrite}')


simulator = args.simulator
if selection and ('msms' not in simulator):
    sys.exit(f'Error: Simulator incompatible with --neutral n: please provide the msms command using --simulator')

print(f'Simulator: {simulator}')
#simulator= 'java -jar /home/fjay/Documents/Softwares/msms/lib/msms.jar'  #'java -jar msms.jar' or 'msms' 'java -jar /home/fjay/Documents/Softwares/msms/lib/msms.jar'

"""

"""
print(f'Demographic scenario: {args.demo}')

data_path = 'Results'
if run_test:
    data_path = os.path.join(data_path,'test')
if selection:
    data_path = os.path.join(data_path,'grid')
else:
    data_path = os.path.join(data_path,'neutral')

if len(num_sample_vec)>1:
    data_path = os.path.join(data_path,'mult-nsam')

print(f'Output parent directory: {data_path}')


sys.path.append("simu/")
# ## Load pretrained SPIDNA

# In[3]:


training_param = utils.load_dict_from_json('pretrained_SPIDNA/pretrained_SPIDNA_training_param.json')
np.random.seed(training_param['seed'])
torch.manual_seed(training_param['seed'])
num_output = len(training_param['param_to_learn'])

# Set specified GPU
device = torch.device('cuda' if torch.cuda.is_available() and training_param['use_cuda'] else 'cpu')
if device.type == 'cuda' and training_param['cuda_device'] is not None:
    torch.cuda.set_device(training_param['cuda_device'])
net = getattr(net_module, training_param['network_name'])
net_param = {'num_output': num_output,
             'num_block': training_param['num_block'],
             'num_feature': training_param['num_feature'],
             'device':device}
net = net(**net_param).to(device)
if device.type=='cpu':
    state_dict = torch.load(os.path.join(training_param['network_path']), map_location=torch.device('cpu'))
else:
    state_dict = torch.load(os.path.join(training_param['network_path']))

# Create new OrderedDict that does not contain `module.`
new_state_dict = OrderedDict()
for k, v in state_dict.items():
    name = k[7:] # remove `module.`
    new_state_dict[name] = v
net.load_state_dict(new_state_dict)

# Dispatch mini-batch on multiple GPUs
if device.type == 'cuda':
    net = torch.nn.DataParallel(net, device_ids=range(torch.cuda.device_count()))


# ## Simulate scenario
#

# ### Define scenarios' parameters

# In[5]:


nb_times = 21
time_rate = 0.06
tmax = 130000
num_time_windows = nb_times  #21 most of the time
population_time = [(np.exp(np.log(1 + time_rate * tmax) * i /
                  (num_time_windows - 1)) - 1) / time_rate for i in
                  range(num_time_windows)]

scenarios_notebook = {
    'Medium': [3.7, 3.7, 3.7, 3.7, 3.7, 3.7, 3.7, 3.7, 3.7, 3.7, 3.7, 3.7, 3.7, 3.7, 3.7, 3.7, 3.7, 3.7, 3.7, 3.7, 3.7],
    'Large': 4.7 * np.ones(shape=21, dtype='float'),
    'Decline': [2.5, 2.5, 3, 3, 3, 3, 3.2, 3.4, 3.6, 3.8, 4, 4.2, 4.6, 4.6, 4.6, 4.6, 4.6, 4.6, 4.6, 4.6, 4.6],
    'Expansion': [4.7, 4.7, 4.7, 4.6, 4.6, 4.5, 4.4, 4.3, 4, 3.7, 3.4, 3.4, 3.4, 3.4, 3.4, 3.4, 3.4, 3.4, 3.4, 3.4, 3.4],
    'Bottleneck': [4.8, 4.8, 4.8, 4.8, 4.8, 4.8, 4.8, 4.8, 4.8, 4.5, 4.15, 3.8, 4.3, 4.8, 4.55, 4.3, 4.05, 3.8, 3.8, 3.8, 3.8],
    'Zigzag': [4.8, 4.8, 4.8, 4.5, 4.15, 3.8, 4.15, 4.5, 4.8, 4.5, 4.15, 3.8, 4.3, 4.8, 4.55, 4.3, 4.05, 3.8, 3.8, 3.8, 3.8]}

# In[6]:


#%%script false --no-raise-error # do not execute when running the whole notebook


# Population sizes are defined on a log10 scale in all_scenarios and scenarios_notebook
scenarios = scenarios_notebook
scenarios = {key: value for key, value in scenarios_notebook.items() if key in [args.demo]} #['Medium', 'Large', 'Decline','Expansion']} # 'Decline' #Expansion
scenarios = {k:10**np.array(v) for k,v in scenarios.items()}

if len(scenarios)==0: #args.demo in scenarios_notebook.keys():
    print(f'Error: scenario {args.demo} is not pre-defined.')
    sys.exit()


# Population sizes are NOT defined on a log10 scale in scenarios_cattle
#scenarios = scenarios_cattle
#scenarios = {key: np.array(value) for key, value in scenarios_cattle.items() if key == 'angus (abc estimates)'}

seed = None #2

# test values

if run_test:
    num_replicates = 2
    num_sample = 20
    fixed_rec_rate = 5e-9 # set to None if you want to randomly draw from the prior
    mutation_rate = 1e-8
    segment_length = 2e5

else:
    num_replicates = 100
    num_sample = 50
    fixed_rec_rate = 5e-9 # set to None if you want to randomly draw from the prior
    mutation_rate = 1e-8
    segment_length = 2e6


N0REF = None
#N0REF = 10000 # might be changed later 100 # this should not matter for simulations without selection # did not work with 1000

savespace=True

complement=None # "1_complement" (if finishing a run that was not finished for memory issues)
# Adding selection?

if selection:
    SAa_vec = [100, 200, 400, 800]  # 2NS
    SAa_to_unscale = True

    sel_start_time_vec = [1e3/5,  5e3/5, 10e3/5] #[1e3/5,10e3/5]
    '''
    time in generation
    10ky ago converted to cattle generation time.
    It will multiplied by 4*N0
    '''

    freq_vec = [0.001, 0.01, 0.05] # [0.01] #[1.1, 0.001, 0.01, 0.05] #[1/num_sample, 0.2]
    ''' initial frequency of allele A (at sel_start_time)
        should be 0 for selection on de novo mutation
        higher for selection on existing beneficial allele (could be the case for selection due to domestication)

        if >1 it is an absolute nb of individuals and will be later transformed to a frequency using the proper N0
    '''

    print("Debug: selection")

else:
    SAa_vec = [0]
    SAa_to_unscale = True
    sel_start_time_vec = [0]
    freq_vec = [0]


# ### Launch simulations


# In[9]:
grid = list(itertools.product(num_sample_vec, SAa_vec, sel_start_time_vec, freq_vec))
grid_size = len(scenarios.keys()) * len(grid)
print(f'Grid of size {grid_size}:')
print(grid)


# In[10]:

if start_scenario_num == -1: # was not specified, compute automatically using grid size
    scenario_num = task_num * grid_size -1
else:
    scenario_num = start_scenario_num -1


print(f'Starting indexing scenarios at {scenario_num+1}')

scenario_names = dict()


# In[11]:

if True: #need to unindent when in a proper editor!
    snp_data = {}
    traces = {}
    sel_params = {}
    for k in scenarios.keys():
        if selection:
            model_name = f'{k}-grid-{task_num}'
        else:
            model_name = f'{k}-neutral-{task_num}'

        ddd = os.path.join(data_path, model_name)
        if os.path.isdir(ddd) and start_scenario_num==-1:
            sys.exit(f'Error: Directory {ddd} already exists, remove before launching new simulations or specify --start_scenario [int]')


        print(f'\n Simulating scenario {model_name}')
        trace_dir = os.path.join(data_path, model_name, 'traces')
        Path(trace_dir).mkdir(parents=True, exist_ok=True)

        population_size = scenarios[k]
        recombination_rate = np.random.uniform(low=1e-9, high=1e-8)  if fixed_rec_rate is None  else fixed_rec_rate

        # loop over parameter grid
        for num_sample, sel_strength, sel_time, freq in grid:
            scenario_num += 1
            npzdir = os.path.join(data_path, model_name,
                                                     f'scenario_{scenario_num}')
            if os.path.isdir(npzdir) and not overwrite:
                sys.exit(f'Error: Directory {npzdir} already exists, specify higher number for scenario indexing with --start_scenario [int] or change overwrite to True, or delete existing directory')

            # setting N0 to the population size at the starting time of selection
            if N0REF is None:
                N0 = population_size[np.argmin(np.abs(np.asarray(population_time) - sel_time ))]
                # print(f'N0 {N0}')
            else:
                N0 = N0REF

            if SAa_to_unscale:
                sel_strength = sel_strength/(2*N0) # it will be multiplied by 2N0 in simulate_ms_stepsize

            if freq>=1: # in this case freq contains in fact absolute count
                freq = freq/N0

            selparam_tmp = dict({'SAA': 2*sel_strength,  # SA and SAa will be multiplied by  2*N0
                               'SAa': sel_strength,  # we assume additive effect of the beneficial mutation
                               'Sp': 0.5,  # position of the mutation (if 0.5 it's in the middle of the region)
                               'SI': f'{sel_time} {1} {freq}', # starting time of selection an initial frequency
                              })
            sel_params_dic = dict({'scenario':k,'SAa':sel_strength, 'seltime':sel_time, 'freq':freq,'N0':N0})
            sim_name =  '_'.join([f'{key}-{value}' for key,value in sel_params_dic.items()])
            sim_name += f'_nsam-{num_sample}'
            sim_name += f'_task-{task_num}'
            sel_params[sim_name] = sel_params_dic
            print(f'* {scenario_num}: {sim_name}')
            extraparam_tmp = {'oTrace': ''}
            if not selection:
                selparam_tmp={}
                extraparam_tmp = {}

            '''simulate'''
            if simulator=='msprime':
                traces = {}
                snp_data[sim_name] = msu.simulate_scenario(population_size,
                                                           population_time,
                                                           seed,
                                                           num_replicates,
                                                           mutation_rate,
                                                           recombination_rate,
                                                           segment_length,
                                                           num_sample)


            else:
                snp_data[sim_name], traces[sim_name] = msu.simulate_ms_stepsize(population_size,
                                                                                population_time,
                                                                                num_replicates,
                                                                                mutation_rate,
                                                                                recombination_rate,
                                                                                segment_length,
                                                                                num_sample,
                                                                                N0=N0,
                                                                                cmd=simulator,
                                                                                n_jobs=4,
                                                                                all_replicates=True,
                                                                                selparam=selparam_tmp,
                                                                                extraparam=extraparam_tmp,
                                                                                fake_precision=True )

                # save trace
                if selection:
                    with open( os.path.join(trace_dir,f'{sim_name}_traces.pkl'), 'wb') as file:
                        pickle.dump(traces, file)


            '''save snp in npz format'''
            #scenario_names[f'{task_num}{scenario}'] = sim_name
            scenario_names[scenario_num] = sim_name
            Path(npzdir).mkdir(parents=True, exist_ok=True)
            for i in range(len(snp_data[sim_name])): # replicates
                np.savez_compressed(os.path.join(npzdir,f'{model_name}_{scenario_num}_{i}'),
                                    SNP=snp_data[sim_name][i][1:,:].astype(np.uint8),
                                    POS=snp_data[sim_name][i][0,:])



            '''infer popsize with spidna'''
            net.eval()
            predictions = pd.DataFrame()
            with torch.no_grad():
                for scenario in snp_data.keys(): # kept the previous loop for convenience
                    all_output = torch.Tensor()
                    for rep, inputs in enumerate(snp_data[scenario]):

                        # take the SNP_min SNPs in the middle:
                        init_size = inputs.shape
                        if selection:
                            # get index of snp closest to the position under selection
                            # (asssuming it was placed in the middle)
                            # central = inputs.shape[1]//2 # central SNP (rounded to lower int)
                            central = np.argmin(np.abs(np.cumsum(inputs[0,]) - 0.5*segment_length))
                            left_bound = central - training_param['SNP_min']//2
                            right_bound = central + training_param['SNP_min']//2
                            inputs = torch.Tensor(
                                inputs[:, left_bound:right_bound]
                            ).to(device).unsqueeze(0)
                        else:
                            # take the first SNPs up to SNP_min:
                            inputs = torch.Tensor(
                                inputs[:,:training_param['SNP_min']]
                            ).to(device).unsqueeze(0)

                        #print(inputs.shape)
                        if inputs.shape[2]<400:
                            print(f"replicate {rep} of scenario {scenario} has size {init_size}. It will be ignored.")
                        else:
                            output = net(inputs).cpu()
                            all_output = torch.cat((all_output, output), 0)
                    pred = pd.DataFrame(all_output.numpy())
                    pred['scenario_name'] = scenario
                    pred['scenario_num'] = scenario_num
                    predictions = pd.concat([predictions, pred])

            predictions.to_csv(
                os.path.join(data_path, model_name, f'{model_name}_predictions.csv'),
                mode='a', header=False)



            if savespace:
                del snp_data, traces, sel_params
                snp_data = {}
                traces = {}
                sel_params = {}


# In[12]:

# saving mapping between scenario names and index/num
pd.Series(scenario_names).to_csv(
    os.path.join(data_path, model_name, f"{model_name}_scenario_names.csv"),
    header=False, mode='a')

b_time = datetime.now()
print(b_time)

# In[13]:

# Compute summary statistics:
# could be called directly rather than by command line if needed
# but this does the joblib
## build command line
cwd = os.getcwd()
data_absolute_path = os.path.join(cwd,data_path,model_name)
arguments = f"--datapath {data_absolute_path}  --nameid {model_name} --workdir simu/"
cmd = f"python simu/summary_statistics.py {arguments}"
print('\n'+cmd)

## compute sumstats for all scenarios in the datapath directory
# (including previously simulated scenarios if they ar in this directory?)
# todo : delete first the sumstats if it exists / change append mode ?, compute only for new scenarios ?
#! {cmd} # in notebook
res = subprocess.check_output(cmd.split())
print(res)
c_time = datetime.now()

# print timings for info
print("\nStarting at")
print(a_time)
print("Simulation and spidna inference done at")
print(b_time)
print("Sum stats computed at")
print(c_time)
