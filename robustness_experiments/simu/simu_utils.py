import numpy as np
import pandas as pd
import random
import re
import warnings
import subprocess
from joblib import Parallel, delayed
import simu.summary_statistics as sumstats 
import matplotlib.pyplot as plt
import seaborn as sns
import msprime

def simulate_ms_stepsize(population_size, population_time, num_replicates, mutation_rate, recombination_rate, segment_length, num_sample, N0=1, cmd='ms', fake_precision=False, all_replicates=True, n_jobs=1, selparam={}, extraparam={}):
    """Simulate single population history using ms or msms.

    Warning: demes, multipop, migration are not supported
    All input times should be given in generations
    They will be divided by 4*N0 to convert them into units of 4*N0generations

    Parameters
    ----------
    population_size : array or list of float
        population size at each step (unit: absolute value)
    population_time : array or list of float
        step starting time (counting backwards) (unit: generation)
        has to be of the same size as population_size
        set both to [] for no size changes ?
    num_sample : int
        =nsam i.e. number of haploid individual to sample.
    num_replicates : int
        =howmany i.e. number of independent replicates (indepdt chromosomes).
    segment_length : float
        Size of each chromosome.
    mutation_rate : float
        Mutation rate per bp per generation.
    recombination_rate : float
        Recombination rate per bp per generation.
    N0 : int
        N0 is used to rescale sizes and population sizes
    cmd : str
        software command line definition
        'ms' or 'msms' or 'java -jar msms.jar'
    fake_precision : bool
        if True: artificially increase the resolution of SNP positions
        (ie slightly modify snp positions locally)
        low resolution in simulations produces SNPs position collusion
        there is still a risk of collusion if you violated the infinite size assumption by having a small segment length and a high mutation rate
    all_replicates : bool
        if True: simulate all replicates at once
        if False: simulate each replicate independently, in parallel
        (maybe dangerous if they are launch simultenaously they might not be independent because the seed is based on the timestamp ?)
    n_jobs : int
        number of job launched in parallel
        used only if all_replicate=False

    Returns
    -------
    out : list(np.array)
        list of length num_replicates containing
        simulated data arrays that fit DNA_DNA network input requirements:
        each replicate is an array with
        - 1st row: distance between SNPs (in bp, encoded as int)
        - remaining rows: snp data (encoded as int8)

    """

    theta = 4 * N0 * mutation_rate * segment_length
    """
    For now the ms output file processing work only for one replicate, so we will produce num_replicates independet ms output
    When read_ms will be updated we can change back to:
    mscmd = f"{cmd} {num_sample} {num_replicates} -t {theta} -p 7 "  # precision for position digits
    """
    mscmd = []
    if all_replicates :
        mscmd.extend([f"{cmd} {num_sample} {num_replicates} -t {theta}"])
    else:
        mscmd.extend([f"{cmd} {num_sample} {1} -t {theta}"])

    if cmd=='ms':
        precision=7
        mscmd.extend([f"-p {precision}"])  # precision for position digits not available in msms?
    else:
        precision=5 # that's msms precision (and cannot be changed)
        mscmd.extend([f"-threads {n_jobs}"])  # this option exists only for msms

    if (recombination_rate != 0):
        rho = 4 * N0 * recombination_rate * (segment_length-1)
        mscmd.extend([f"-r {rho} {int(segment_length)}"])

    if len(population_size) != len(population_time):
        raise ValueError(
            "population_time and population_size vectors are required to have the same length")

    if len(population_size)!=len(population_time):
        raise ValueError('population_time and population_time are required to have the same length')
    if not population_time is None and len(population_time)!=0:
        mscmd.extend([f"-eN {time/(4*N0)} {size/N0}" for time,size in zip(population_time, population_size)])

    if len(selparam)!=0:
        # Adding selection
        # selection require to specify N0
        mscmd.extend([f'-N {N0}'])

        scale= dict(zip(selparam.keys(),[1]*len(selparam))) # no scaling by default
        scale['SAA'], scale['SAa'] = 2*N0, 2*N0  # 2N0 according to the manual, ?!4N0 on msms website?!
        scale['Smu'], scale['Snu']= 4*N0, 4*N0
        mscmd.extend([f"-{key} {val*scale[key]}" for key,val in selparam.items() if key!='SI'])
        if 'SI' in selparam.keys():
            #'SI': f'{sel_start_time} {1} {freq}'
            # sel_start_time needs to be rescaled
            si = selparam['SI'].split()
            # time is converted to units of (4*N0) generations
            mscmd.extend([f"-SI {float(si[0])/(4*N0)} {si[1]} {si[2]}"])

    mscmd.extend([f"-{key} {value}" for key, value in  extraparam.items()])

    mscmd = ' '.join(mscmd)
    print(mscmd)
    if all_replicates:
        res = subprocess.check_output(mscmd.split())
        alld, list_trace = read_ms(ms_res=res, snpformat='np')
    else:
        allres = Parallel(n_jobs=n_jobs)(delayed(subprocess.check_output)(mscmd.split()) for rep in range(num_replicates))
        alld, list_trace = zip(* [read_ms_onerep(ms_res=res, snpformat='np') for res in allres])  #DNA_DNA package format

    if not fake_precision:
        # resolution/precision of snp position is not artificially increased
        # snp positions are untouched
        precision = np.inf
    out = [snpdic_to_netinput(d, segment_length, precision) for d in alld] #DNA_DNA network input format
    if all(v is None for v in list_trace): # there was no frequency trace in the output
        list_trace = None
    return out, list_trace

def simulate_from_cmd(mscmd, segment_length, all_replicates=True, num_replicates=None, n_jobs=1, precision=np.inf):
    print(mscmd)
    if all_replicates:
        res = subprocess.check_output(mscmd.split())
        alld, list_trace = read_ms(ms_res=res, snpformat='np')
    else:
        allres = Parallel(n_jobs=n_jobs)(delayed(subprocess.check_output)(mscmd.split()) for rep in range(num_replicates))
        alld, list_trace = zip(* [read_ms_onerep(ms_res=res, snpformat='np') for res in allres] )  #DNA_DNA package format

    out = [snpdic_to_netinput(d, segment_length, precision) for d in alld] #DNA_DNA network input format
    if all(v is None for v in list_trace): # there was no frequency trace in the output
        list_trace = None
    return out, list_trace

def simulate_scenario(population_size, population_time, seed, num_replicates, mutation_rate, recombination_rate,
                      segment_length, num_sample):
    """simulate with msprime

    Author: theophile
    """

    demographic_events = [msprime.PopulationParametersChange(
                time=population_time[i],
                growth_rate=0,
                initial_size=population_size[i]) for i in range(1, len(population_time))]

    population_configurations = [msprime.PopulationConfiguration(
                sample_size=num_sample,
                initial_size=population_size[0])]

    tree_sequence = msprime.simulate(
                length=segment_length,
                population_configurations=population_configurations,
                demographic_events=demographic_events,
                recombination_rate=recombination_rate,
                mutation_rate=mutation_rate,
                num_replicates=num_replicates,
                random_seed=seed)
    pos = []
    snp = []
    for i, rep in enumerate(tree_sequence):
                positions = [variant.site.position for variant in rep.variants()]
                positions = np.array(positions) - np.array([0] + positions[:-1])
                positions = positions.astype(int)
                pos.append(positions)
                SNPs = rep.genotype_matrix().T.astype(np.uint8)
                snp.append(SNPs)
    data = [[snp[i], pos[i]] for i in range(len(snp))]
    data = [np.vstack([d[1], d[0]]) for d in data]
    return data


def snpdic_to_netinput(d, segment_length, precision=np.inf):
    """ reformat output of one replicate simulated by ms or msms to suit DNA_DNA network input format

    Parameters
    ----------
    d : dict('snp': np.array(int8), 'pos':np.array(float) )
        Dictionnary containg SNP data and SNP positions of one replicate
    segment_length: int
        Length (bp) of each simulated region
    precision: float
        Low resolution in simulations produces SNPs position collusion
        so we propose to artificially increase the resolution
        (ie slightly modify snp positions locally if needed)
        there is still a risk of collusion (if you violated the infinite size assumption by having a small length and a high mutation rate)
        np.inf : keep original positions
        4: default precision in ms
        5: default precision in msms
        7: ms precision if you used our simulator wrapping function simulate_ms_stepsize()
        ms precision can be changed with option -p, which we set to 7 in
        To my knowledge it cannot be change in msms.

    Returns
    -------
     : np.array
         of size (1+num_samples)x nsnp
         simulated data arrays that fit DNA_DNA network input requirements:
         - 1st row: distance between SNPs (in bp, encoded as int)
         - remaining rows: snp data (encoded as int8)

    """
    positions = np.array(d['pos']) * segment_length  # convert from relative position in [0,1] to bp position
    resolution_bp = int(10**(-precision)*segment_length)
    if resolution_bp>1:
        artif_shift_bp = np.array([random.randrange(resolution_bp) for s in range(len(positions))])
        positions = np.sort(positions + artif_shift_bp)

    positions = np.insert(np.diff(positions),0, positions[0], axis=0)
    positions = positions.astype(int)
    return np.vstack((positions,d['snp']))



def plot_sumstats(haplotype, pos_vec, key, pdf=None, window=250):
    fig, axs = plt.subplots(ncols=3,  figsize=(15,5))

    print("plotting for", key)

    tajwin = sumstats.tajimasD(haplotype, pos_vec=pos_vec, window=window)
    avetaj = sumstats.tajimasD(haplotype)

    ax = axs[0]
    ax.plot(tajwin,'.',)
    ax.axhline(0)
    ax.set_ylabel('Tajima D')
    ax.set_xlabel('window along genome')
    ax.set_title('Tajima D')

    plt.suptitle(f'{key} - ave TajD {round(avetaj,3)}')

    ax = axs[1]
    ax.plot(sumstats.ihs(haplotype=haplotype, pos_vec=pos_vec, window=window),'.')
    ax.axhline(0)
    ax.set_ylabel('ihs')
    ax.set_xlabel('window along genome')
    ax.set_title('IHS')

    ax = axs[2]
    sfs_tmp = sumstats.sfs(haplotype=haplotype,ac=haplotype.count_alleles())
    ave_i_xi = sfs_tmp['i_xi'].sum()/haplotype.shape[1]
    sfs_tmp['i_xi_norm'] = sfs_tmp['i_xi']/ ave_i_xi
    g = sns.relplot(data=sfs_tmp, x='N_indiv', y='i_xi_norm', ax=ax)
    ax.axhline(1)
    ax.set_title('SFS norm')

    plt.close(g.fig)
    if pdf is None: return
    pdf.savefig() # outdirpath+ f'sumstats_{key}.pdf')
    plt.close()



def read_ms(fname=None, ms_res=None, load_snps=True, snpformat="np"):

    """read ms file or ms results output of subprocess.

    Loading all replicates

    Parameters
    ----------
    fname : str
        Name of file containing ms output.
    ms_res : bytes or str
        Name of object containing ms output (results of subprocess).
    load_snps : bool
        Load snp data? If False only postions are loaded
    snpformat : str
        Format of SNP matrix
         "numpy" or "np" -> numpy.ndarray
         "pandas" or "pd" -> pandas.core.frame.DataFrame

    Returns
    -------
    list_reps  list(dict('snp': np.array(int8), 'pos':np.array(float) ))
        List of size num_replicates. For each rep:
        dictionnary containg SNP data and SNP positions
        if snpformat=="pd", snp is saved as a pd.core.frame.DataFrame

    """

    if all(v is None for v in {fname, ms_res}):
        raise ValueError('Expected either fname or ms_res args')

    if all(v is not None for v in {fname, ms_res}):
        raise ValueError('Specify fname or ms_res arg, but not both')

    if snpformat not in ["numpy", "np", "pandas", "pd"]:
        raise ValueError('Unreckognized argument value for snpformat {}'
                         .format(snpformat))

    if fname is not None:
        file_in = open(fname)
    else:
        if ms_res.__class__ is bytes:
            ms_res = ms_res.decode('utf8')
        file_in = ms_res.split('\n')



    cmd_pattern = r"^[^\s]*ms"
    pos_ok, trace_ok = False, False
    rep = -1

    for il, line in enumerate(file_in):
        if re.match(cmd_pattern, line):
            # line giving the command line, appears only once
            n_samples, n_rep = [int(x) for x in line.split(' ')[1:3]] #TODO FIX msms can also be called as follows ms -N 100 nsam nrep so this line is not general enough
            list_rep = [None]*n_rep  # List of predefined size -> save a bit of time compared to append ?
            list_trace = [None]*n_rep
            # print(n_samples,n_rep)

        elif line.startswith("//"):
            # new replicate
            sample=0
            rep +=1
            snps = None # will stay at None if load_snps is False
            positions = None
            frequency_trace = None
            pos_ok = False
            trace_ok = False

        elif line.startswith("Frequency Trace"):
            frequency_trace=[]
            trace_ok = True

        elif line.startswith("segsites"):
            num_segsites = int(line.split()[1])
            trace_ok = False
            if num_segsites == 0:
                list_rep[rep] = dict({'snp': None, 'pos': None})

        elif line.startswith("positions"):
            positions = np.array([float(j) for j in line.split()[1:]])
            pos_ok = True
            if load_snps and num_segsites != 0:
                snps = pd.DataFrame(columns=range(num_segsites), dtype=int)

        elif trace_ok and line.strip(): # skip lines having only whitespace
            frequency_trace.append([float(j) for j in line.split()])

        elif pos_ok and line.strip():
            # print(sample, end=' ')
            if load_snps and num_segsites != 0:
                snps.loc[sample] = np.array([int(j) for j in line.split()[0]])
            sample += 1
            if sample == n_samples:
                pos_ok = False # this avoids interpreting following lines as snps while they are space or other unpredicted strings
                if load_snps and snpformat in ["numpy", "np"]:
                    list_rep[rep] = dict({'snp': np.array(snps, dtype='int'), 'pos': positions}) # replace int8 by int to check wether it can now read msms with Smark
                else:
                    list_rep[rep] = dict({'snp': snps, 'pos': positions})
                if not frequency_trace is None:
                    list_trace[rep] = np.array(frequency_trace)

    if rep != n_rep-1:
        warnings.warn(f"Expecting {n_rep} replicates, read {rep}")

    if fname is not None:
        file_in.close()

    #if all(v is None for v in list_trace): # there was no frequency trace in the output
    #    list_trace = None

    return list_rep, list_trace


'''Not used anymore'''
def read_ms_onerep(fname=None, ms_res=None, load_snps=True, snpformat="np"):
    """read the first replicate of ms file or ms results output of subprocess.
    better to use the generic version read_ms that loads all replicates

    Warning: with ONE replicate only
    Warning: Not efficient for big files bc all the SNPs are charged
            in memory at once.


    Parameters
    ----------
    fname : str
        Name of file containing ms output.
    ms_res : bytes or str
        Name of object containing ms output (results of subprocess).
    load_snps : bool
        Load snp data? If False only postions are loaded
    snpformat : str
        Format of SNP matrix
         "numpy" or "np" -> numpy.ndarray
         "pandas" or "pd" -> pandas.core.frame.DataFrame

    Returns
    -------
    dict('snp': np.array(int8), 'pos':np.array(float) )
        Dictionnary containg SNP data and SNP positions
        if snpformat=="pd", snp is saved as a pd.core.frame.DataFrame

    """

    if all(v is None for v in {fname, ms_res}):
        raise ValueError('Expected either fname or ms_res args')

    if all(v is not None for v in {fname, ms_res}):
        raise ValueError('Specify fname or ms_res arg, but not both')

    if snpformat not in ["numpy", "np", "pandas", "pd"]:
        raise ValueError('Unreckognized argument value for snpformat {}'
                         .format(snpformat))

    if fname is not None:
        f = open(fname)
    else:
        if ms_res.__class__ is bytes:
            ms_res = ms_res.decode('utf8')
        f = ms_res.split('\n')

    pattern = r"^[^\s]*ms"  # correspond to the line giving the command line
    for il, line in enumerate(f):
        if re.match(pattern, line):
            nsamples = int(line.split(' ')[1])
        if line.startswith('segsites'):
            segsites = int(line.split(':')[1])
        if line.startswith('positions'):
            # get position vector and then stop reading line by line
            positions = np.array(line.split(':')[1].split(), dtype=float)
            break

    if load_snps:
        if fname is not None:
            snps = pd.read_fwf(fname, header=None, skiprows=il+1, sep='',
                               widths=[1]*segsites, nrows=nsamples,
                               dtype='int8')
            # snps were loaded as a panda dataframe
            # convert to np.array if needed:
            if snpformat in ["numpy", "np"]:
                snps = np.array(snps, dtype='int8')
        else:
            snps = [int(s) for g in f[(il+1):(il+1+nsamples)] for s in g]
            snps = np.array(snps, dtype='int8').reshape((nsamples, segsites))
            if snpformat in ["pandas", "pd"]:
                snps = pd.DataFrame(snps, dtype='int8')
    else:
        snps = None

    if fname is not None:
        f.close()
    return dict({'snp': snps, 'pos': positions})
