#!/bin/bash

# All in test mode (ie 1 replicate per experiment, few simulated individuales and nb of regions, small region sizes)
# It runs multiple experiments together, and is just a test (option --test y)
# See ./launch_nsam.sh and ./launch_sel_grid.sh for complete experiments
# usage: ./launch_test.sh Decline
# or  ./launch_test.sh Medium expansion
# aso
# for msms simulator use: --simulator "java -jar simu/msms3.2rc_b163.jar"
#
# check more options with
# python simulate_grid_selection_neutral_nsam.py --help


for demo in "$@"
do

   echo $demo
  logdir="Logs/test_$demo"
  mkdir -p $logdir

  for ((i=0; i<1; i++)) do
    echo $i
    echo "neutral, msprime, varying nsam"
    #python simulate_grid_selection_neutral_nsam.py --task $i --demo $demo --neutral y --nsam 35 45 55 65 --start_scenario 6 > $logdir/log$i.txt 2>$logdir/err$i.txt
    python simulate_grid_selection_neutral_nsam.py --task $i --demo $demo --neutral y --nsam 10 20  --simulator msprime --test y > $logdir/log$i.txt 2>$logdir/err$i.txt

    echo "neutral, using msms (default)"
    python simulate_grid_selection_neutral_nsam.py --task $i --demo $demo --neutral y --test y >> $logdir/log$i.txt 2>>$logdir/err$i.txt

    echo "selection grid"
    # selection grid (16 replicates)
    python simulate_grid_selection_neutral_nsam.py --task $i --demo $demo --neutral n --test y >> $logdir/log$i.txt 2>>$logdir/err$i.txt

  done
done
