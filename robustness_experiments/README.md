* At first trial run:  
`python simulate_grid_selection_neutral_nsam --help `   
to have insights on the script parameters

* Launch a test experiment with few sample size and small region Length using:  
`./launch_test.sh  ` 

* Launch the paper robustness experiments using:  
`./launch_nsam.sh  `  
or  
`./launch_sel_grid.sh  `  


* Create your own experiment by changing the command line  
(or the selection parameters directly in the python script)  
