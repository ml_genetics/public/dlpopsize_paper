#!/bin/bash

# Runs sel grid experiment (first neutral simulation then with selection)
# Selection parameter grid is hard coded in simulate_grid_selection_neutral_nsam.py
# and can be changed if needed
#
# usage: ./launch_sel_grid.sh Decline
# or  ./launch_sel_grid.sh Medium expansion
# aso
# for msms simulator use: --simulator "java -jar simu/msms3.2rc_b163.jar"
#
# check more options with
# python simulate_grid_selection_neutral_nsam.py --help

for demo in "$@"
do

  # neutral (30 replicates)
  logdir=Results/neutral/logdir_$demo-grid
  mkdir -p $logdir
  for ((i=0; i<30; i++)) do #30
      echo $i;
      python simulate_grid_selection_neutral_nsam.py --task $i --demo $demo --neutral y --test n > $logdir/log$i.txt 2>$logdir/err$i.txt
  done


  # selection grid (16 replicates)
  logdir=Results/grid/logdir_$demo-grid
  mkdir -p $logdir

  for ((i=0; i<16; i++)) do  #16
      echo $i;
      python simulate_grid_selection_neutral_nsam.py --task $i --demo $demo --neutral n --test n > $logdir/log$i.txt 2>$logdir/err$i.txt
  done

done
