#!/bin/bash

# Runs sel neutral msprime varying sample size experiment
#
# usage: ./launch_nsam.sh Decline
# or  ./launch_nsam.sh Medium expansion
# aso
# for msms simulator use: --simulator "java -jar simu/msms3.2rc_b163.jar"
#
# check more options with
# python simulate_grid_selection_neutral_nsam.py --help


for demo in "$@"
do
   echo $demo

    logdir="Results/neutral/mult-nsam/logdir_$demo"
    mkdir -p $logdir

    for ((i=0; i<1; i++)) do
      echo $i
      #python simulate_grid_selection_neutral_nsam.py --task $i --demo $demo --neutral y --nsam 35 45 55 65 --start_scenario 6 > $logdir/log$i.txt 2>$logdir/err$i.txt
      python simulate_grid_selection_neutral_nsam.py --task $i --demo $demo --neutral y --nsam 10 25 35 40 45 50 55 60 65 75 100 150  --simulator msprime  > $logdir/log$i.txt 2>$logdir/err$i.txt
    done

done


# for msms simulator use: --simulator "java -jar simu/msms3.2rc_b163.jar"
