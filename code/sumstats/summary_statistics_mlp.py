import pandas as pd
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader, sampler
import sys
sys.path.append('/home/tau/thsanche/dnadna/dl')
import utils

summary_statistics = pd.read_csv('/home/tau/thsanche/data/cattle/summary_statistics/cattle_summary_statistics_no_maf_030819.csv')
preprocessed_parameters_path = '/home/tau/thsanche/run/cattle/run_0/cattle_run_0_preprocessed_param.csv'
training_param_path = '/home/tau/thsanche/run/cattle/run_0/cattle_run_0_training_param.json'
output_path = '/home/tau/thsanche/dnadna/mlp_res_random_search.csv'
loader_num_workers = 20
use_cuda = True
num_epoch = 6
evaluation_interval = 400
budget = 1e7/100
summary_statistics_col = [name for name in summary_statistics.columns if 'count_' in name or 'mean_r2' in name]
training_param = utils.load_dict_from_json(training_param_path)

res = pd.DataFrame(columns=['learning_rate', 'batch_size', 'weight_decay'] + training_param['param_2_learn'])
res.to_csv(output_path)

#learning_rate = [0.00005, 0.005025, 0.01]
#weight_decay = [0.00005, 0.005025, 0.01]
#batch_size = [10, 55, 100]
#grid = [[lr, wd, bs] for lr in learning_rate for wd in weight_decay for bs in batch_size]

grid = [[np.random.uniform(0.00005, 0.01), np.random.uniform(0.00005, 0.01), np.random.randint(10, 100)] for i in range(3**3)]

class DNADataset(Dataset):
    def __init__(self, preprocessed_parameters, summary_statistics, summary_statistics_col, param_2_learn):
        self.preprocessed_parameters = preprocessed_parameters
        self.summary_statistics = summary_statistics
        self.size = len(self.preprocessed_parameters)
        self.summary_statistics_col = summary_statistics_col
        self.param_2_learn = param_2_learn
    
    def __getitem__(self, index):
        targets = np.array(self.preprocessed_parameters[self.preprocessed_parameters['scenario_idx'] == index][self.param_2_learn])
        inputs = np.array(self.summary_statistics[self.summary_statistics['scenario'] == index][self.summary_statistics_col])
        #inputs = np.array([inputs[0]]) #
        targets = torch.Tensor(targets).squeeze(0)
        inputs = torch.Tensor(inputs).squeeze(0)
        
        return inputs, targets
    
    def __len__(self):
        return self.size


preprocessed_parameters = pd.read_csv(preprocessed_parameters_path)
training_indices = preprocessed_parameters['scenario_idx'].loc[np.array(preprocessed_parameters['training_set'])]
validation_indices = preprocessed_parameters['scenario_idx'].loc[np.array(preprocessed_parameters['training_set']==False)]
print(f'They are {len(validation_indices)} data in the validation set and {len(training_indices)} in the training set.')
train_sampler = sampler.SubsetRandomSampler(list(training_indices))
validation_sampler = sampler.SubsetRandomSampler(list(validation_indices))
col_2_keep = ['scenario_idx']
col_2_keep.extend(training_param['param_2_learn'])
preprocessed_parameters = preprocessed_parameters[col_2_keep]
 
dataset = DNADataset(preprocessed_parameters, summary_statistics, summary_statistics_col, training_param['param_2_learn'])

class SummaryStatisticsMLP(nn.Module):
    def __init__(self, num_inputs, num_outputs):
        super(SummaryStatisticsMLP, self).__init__()
        self.fc1 = nn.Linear(num_inputs, 25)
        self.batch_norm1 = nn.BatchNorm1d(25)
        self.fc2 = nn.Linear(25, 25)
        self.batch_norm2 = nn.BatchNorm1d(25)
        self.fc3 = nn.Linear(25, 10)
        self.batch_norm3 = nn.BatchNorm1d(10)
        self.fc4 = nn.Linear(10, num_outputs)

    def forward(self, x):
        #x = x.contiguous()
        x = x.view(x.size(0), -1)
        x = F.relu(self.batch_norm1(self.fc1(x)))
        x = F.relu(self.batch_norm2(self.fc2(x)))
        x = F.relu(self.batch_norm3(self.fc3(x)))
        x = self.fc4(x)

        return x
    

for learning_rate, weight_decay, batch_size in grid:
    train_loader = DataLoader(dataset=dataset,
                          batch_size=batch_size,
                          sampler=train_sampler,
                          num_workers=loader_num_workers,
                          pin_memory=use_cuda)

    validation_loader = DataLoader(dataset=dataset,
                                   batch_size=batch_size,
                                   sampler=validation_sampler,
                                   num_workers=loader_num_workers,
                                   pin_memory=use_cuda)
    net = SummaryStatisticsMLP(len(summary_statistics_col), 21)
    best_loss = 1
    device = torch.device('cuda' if torch.cuda.is_available and use_cuda else 'cpu')
    criterion = nn.MSELoss(reduction='none').to(device)

    net = SummaryStatisticsMLP(len(summary_statistics_col), len(training_param['param_2_learn'])).to(device)

    optimizer = optim.Adam(net.parameters(), lr=learning_rate, weight_decay=weight_decay)
    net.train()
    for e in range(num_epoch):
        print(f'Epoch {e}')
        for i, (inputs_train, targets_train) in enumerate(train_loader):
            inputs_train, targets_train = inputs_train.to(device), targets_train.to(device)
            outputs_train = net(inputs_train)
            training_step = inputs_train.shape[0] * i + e * len(train_loader)
            train_loss = criterion(outputs_train, targets_train)
            train_loss = torch.mean(train_loss)
            if (inputs_train.shape[0] * i) % evaluation_interval < inputs_train.shape[0]:
                net.eval()
                with torch.no_grad():
                    all_loss = torch.Tensor()
                    for j, (inputs_eval, targets_eval) in enumerate(validation_loader):
                        inputs_eval, targets_eval = inputs_eval.to(device), targets_eval.to(device)
                        outputs_eval = net(inputs_eval)
                        eval_loss = criterion(outputs_eval, targets_eval)
                        eval_loss = torch.mean(eval_loss, 0)
                        all_loss = torch.cat((all_loss, eval_loss.cpu().unsqueeze(0)), 0)
                    if torch.mean(all_loss) < best_loss:
                        best_loss = torch.mean(all_loss)
                        best_all_loss = torch.mean(all_loss, 0)
                    if training_step>budget:
                        break
            
            train_loss.backward()
            optimizer.step()
            optimizer.zero_grad()
    print(list(best_all_loss.numpy()))
    print([batch_size, learning_rate, weight_decay] + list(best_all_loss.numpy()))
    print(res)
    res.loc[len(res)] = [learning_rate, batch_size, weight_decay] + list(best_all_loss.numpy())
    res.loc[[len(res)-1]].to_csv(output_path,  mode='a', header=False)
