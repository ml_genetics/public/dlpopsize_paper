#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import os
try:
    import allel
except ImportError:
    pass
from scipy.spatial.distance import squareform
import itertools as it
import matplotlib.pyplot as plt
import matplotlib.colors
import seaborn as sns
import warnings
import sys
sys.path.append('/home/tau/thsanche/dnadna')
sys.path.append('/home/tau/thsanche/dnadna/dl')
try:
    import ghalton
except ImportError:
    pass

warnings.simplefilter('ignore', FutureWarning)
warnings.simplefilter('ignore', RuntimeWarning)
import re
import logging
import utils
import glob
import re
import argparse
import multiprocessing

def harmonic_nm1(n):
    """return the n-1 harmonic number"""

    return sum([1/(i-1) for i in range(2, n+1)])


def convert_ms(msfile, n_samples=None):
    """
    Read ms file, which contains:
        //
        segsites: 13
        positions: 0.01 0.2 ...
        00100
        00010
    write them on disk in compressed numpy format.
    """

    df = None
    positions = None
    pos_ok = False
    with open(msfile, "r") as file_in:
        sample = 0
        for i, line in enumerate(file_in):
            if "segsites:" in line.split():
                num_segsites = int(line.split()[1])
                if num_segsites == 0:
                    return
            elif "positions:" in line.split():
                positions = np.array([float(j) for j in line.split()[1:]])
                df = pd.DataFrame(columns=range(num_segsites), dtype=int)
                pos_ok = True
            elif pos_ok and line!="\n":
                df.loc[sample] = np.array([int(j) for j in line.split()[0]])
                sample += 1
                if sample == n_samples:
                    break
    if "msin" in msfile:
        outfile = os.path.splitext(msfile)[0] + "_ms"
        # remove columns without snp (because not present in the subsample)
        d = df.values
        # get columns where at least one element in the column is different from the first element
        positions = positions[(d!=d[0]).any(axis=0)]
        d = d[:, (d!=d[0]).any(axis=0)].astype(int)
    else:
        outfile = os.path.splitext(msfile)[0]
        d = df.values.astype(int)
    np.savez_compressed(outfile,
                        SNP=d,
                        POS=positions)

def read_ms_compressed(npzfile, key="all"):
    """
    Takes a .npz file and return all data (SNP and position).
    If one want to get only SNP matrix, set key="SNP",
    or key="POS" for only Position arrays.
    """
    data = np.load(npzfile)
    data = dict(zip((k for k in data), (data[k] for k in data)))
    if any(np.diff(data["POS"]) < 0):
        data["POS"] = np.cumsum(data["POS"])
    if key=="all":
        return data["SNP"], data["POS"]
    else:
        return data[key]

def split_simid(simid):
    """
    Given a sim_ID looking like: "model-A_N_X.extension",
    return the model name (model-A), the scenario number (N) and the replicate number (X)

    Parameters
    ----------
    simid : str
        simulation ID of the form "model-A_N_X.npz".

    Returns
    -------
    model : str
        name of the model.
    scenario : int
        scenario number.
    replicat : int
        replicat number.
    run_id : str
        run id is the N_X part

    Raise
    -----
    NameError :
        If the simulation ID can't be parsed.
    """

    try:
        model, scenario, replicat = re.findall("^([\w\d\-]+)_(\d+)_(\d+).*", simid)[0]
    except IndexError:
        raise NameError("The name of the simulation should be of the form `model-A_N_X.extension`")
    return model, int(scenario), int(replicat), "{}_{}".format(scenario, replicat)

def sfs(haplotype, ac, nindiv=None, folded=False):
    """
    Compute sfs for SNP matrix
    """
    if nindiv == None:
        nindiv = haplotype.shape[1]
    tmp_df = pd.DataFrame({"N_indiv":range(1, nindiv)})
    if folded:
        df_sfs = pd.DataFrame(allel.sfs_folded(ac), columns=["count_SNP"])
        df_sfs["i_xi"] = allel.sfs_folded_scaled(ac)
        df_sfs.index.name = "N_indiv"
        df_sfs.reset_index(inplace=True)
        df_sfs = df_sfs.merge(tmp_df, on="N_indiv", how="right").fillna(0).astype(int)
    else:
        df_sfs = pd.DataFrame(allel.sfs(ac.T[1]), columns=["count_SNP"])
        df_sfs["i_xi"] = allel.sfs_scaled(ac.T[1])
        df_sfs.index.name = "N_indiv"
        df_sfs.reset_index(inplace=True)
        df_sfs = df_sfs.merge(tmp_df, on="N_indiv", how="right").fillna(0).astype(int)

    df_sfs["freq_indiv"] = df_sfs.N_indiv / nindiv
    return df_sfs

def LD(haplotype, pos_vec, size_chr, circular=True, distance_bins=None):
    """
    Compute LD for a subset of SNPs drawn with different gap sizes in between them.
    Gap sizes follow power 2 distribution.
    The LD is then computed and averaged over different bin (distance_bins) sizes.

    Parameters
    ----------
    haplotype : numpy 2D array or allel.haplotype
        SNP matrix where in the first dimension are the SNP (rows) and
        in the second dimension (columns) are the samples.
    pos_vec : 1D array
        array of absolute positions in [0, size_chr].
    size_chr : int
        Size of the chromosome.
    circular : bool
        Whether to consider the chromosome circular or not.
        If circular, the maximum distance between 2 SNPs is thus half the chromosome.
    distance_bins : int or list
        LD will be averaged by bins of distances
        e.g. if distance_bins = [0, 100, 1000, 10000], LD will be averaged for the groups [0,100[, [100, 1000[, and [1000, 10000[
        If distance_bins is an int, it defines the number of bins of distances for which to compute the LD
            The bins are created in a logspace
        If distance_bins is a list, they will be used instead

    Returns
    -------
    DataFrame
        Table with the distance_bins as index, and the mean value of
    """

    if distance_bins == None or isinstance(distance_bins, int):
        if isinstance(distance_bins, int):
            n_bins = distance_bins - 1
        else:
            n_bins = 19
        if circular:
            distance_bins = np.logspace(2, np.log10(size_chr//2), n_bins)
            distance_bins = np.insert(distance_bins, 0, [0])
        else:
            distance_bins = np.logspace(2, np.log10(size_chr), n_bins)
            distance_bins = np.insert(distance_bins, 0, [0])

    n_SNP, n_samples = haplotype.shape
    gaps = (2 ** np.arange(0, np.log2(n_SNP), 1)).astype(int) # log2 scales of intervals
    #gaps = np.arange(1, n_SNP-1)
    selected_snps = []
    for gap in gaps:

        snps = np.arange(0, n_SNP, gap) + np.random.randint(0, (n_SNP - 1) % gap + 1)  # adding a random start (+1, bc 2nd bound in randint is exlusive)

        # non overlapping contiguous pairs
        # snps=[ 196, 1220, 2244] becomes
        # snp_pairs=[(196, 1220), (1221, 2245)]
        snp_pairs = np.unique([((snps[i] + i) % n_SNP, (snps[i + 1] + i) % n_SNP) for i in range(len(snps) - 1)], axis=0)

        # If we don't have enough pairs (typically when gap is large), we add a random rotation until we have at least 300)
        #count = 0

        if not circular:
            snp_pairs = snp_pairs[snp_pairs[:, 0] < snp_pairs[:, 1]]
        last_pair = snp_pairs[-1]

        if circular:
            max_value = n_SNP - 1
        else:
            max_value = n_SNP - gap - 1

        while len(snp_pairs) <= min(300, max_value):
            #count += 1
            #if count % 10 == 0:
                #print(">>  " + str(gap) + " - " + str(len(np.unique(snp_pairs, axis=0))) + " -- "+ str(len(snps) - 1) + "#" + str(count))
            #remainder = (n_SNP - 1) % gap if (n_SNP - 1) % gap != 0 else (n_SNP - 1) // gap
            random_shift =  np.random.randint(1, n_SNP) % n_SNP
            new_pair = (last_pair + random_shift) % n_SNP
            snp_pairs = np.unique(np.concatenate([snp_pairs,
                                                  new_pair.reshape(1, 2) ]), axis=0)
            last_pair = new_pair

            if not circular:
                snp_pairs = snp_pairs[snp_pairs[:, 0] < snp_pairs[:, 1]]

        selected_snps.append(snp_pairs)


    # Functions to aggregate the values within each distance bin
    agg_bins = {"snp_dist": {"mean_dist":"mean"}, "r2":{"mean_r2":"mean", "Count":"count", "sem_r2":"sem"}}

    ld = pd.DataFrame()
    for i, snps_pos in enumerate(selected_snps):

        if circular :
            sd = pd.DataFrame(((np.diff(pos_vec[snps_pos]) % size_chr))%(size_chr//2), columns=["snp_dist"]) # %size_chr/2 because max distance btw 2 SNP is size_chr/2
        else:
            sd = pd.DataFrame((np.diff(pos_vec[snps_pos])), columns=["snp_dist"])

        sd["dist_group"] = pd.cut(sd.snp_dist, bins=distance_bins)
        sr = [allel.rogers_huff_r(snps) ** 2 for snps in haplotype[snps_pos]]
        sd["r2"] = sr
        sd["gap_id"] = i
        ld = pd.concat([ld, sd])

    ld2 = ld.dropna().groupby("dist_group").agg(agg_bins)
    ld2.columns = ld2.columns.get_level_values(1)
    #ld2.dropna().plot(x="mean_dist", y="mean_r2", yerr="sem_r2")
    return ld2[['mean_dist', 'mean_r2', 'Count', 'sem_r2']]


def tajimasD(haplotype, pos_vec=None, window=None):
    """
    Given a snp_mat, return tajima's D
    window: Number of window of equal size to slice the position vector.

    if windowed stat, provide pos_vec too.
    """
    if not window:
        allel_count = haplotype.count_alleles()
        return allel.diversity.tajima_d(allel_count)
    else:
        all_tajD = []
        pos = pd.DataFrame(pos_vec, columns=["pos"])
        pos["pos_cat"] = pd.cut(pos.pos, 100, labels=range(1, 101))
        pos.index.name = "SNP"
        snp_per = pos.reset_index().groupby("pos_cat").SNP.unique()
        for per in snp_per:
            if len(per):
                allel_count = haplotype[per].count_alleles()
                all_tajD.append(allel.diversity.tajima_d(allel_count))
            else:
                all_tajD.append(np.nan)
        return pd.DataFrame(all_tajD, columns=["TajD"])


def ihs(haplotype, pos_vec, window=None):
    """Compute the standardize integrated haplotype score"""

    ihs = allel.ihs(haplotype, pos_vec, min_maf=0.01,
                    include_edges=True)
    ihs_stand, bins = allel.standardize_by_allele_count(ihs,
                                                        haplotype.count_alleles().T[1],
                                                        diagnostics=False)
    if window:
            di = pd.DataFrame(ihs_stand, columns=["iHS"])
            di["pos_cat"] = pd.cut(pos_vec, window, labels=range(1, window + 1))
            dig = di.groupby("pos_cat").iHS.mean()
            return dig
    else:
        return ihs_stand

def nsl(haplotype, pos_vec=None, window=None):
    """
    Compute the standardize number of segregating sites by length (nSl)
    for each variant, comparing the reference and alternate alleles,
    after Ferrer-Admetlla et al. (2014)

    if windowed stat, provide pos_vec too.
    """

    nsl = allel.nsl(haplotype)
    nsl_stand, bins = allel.standardize_by_allele_count(nsl,
                                                        haplotype.count_alleles().T[1],
                                                        diagnostics=False)
    if window:
        dn = pd.DataFrame(nsl_stand, columns=["nSL"])
        dn["pos_cat"] = pd.cut(pos_vec, window, labels=range(1, window + 1))
        dng = dn.groupby("pos_cat").nSL.mean()
        return dng
    else:
        return nsl_stand

def worker_do_sum_stats(param):
    do_sum_stats(**param)

def do_sum_stats(scenario_dir, name_id, size_chr=2e6, circular=True, label="", nrep="all", one_file=False, overwrite=False):
    all_sfs = pd.DataFrame()
    all_ld = pd.DataFrame()
    npzfiles = [i for i in os.listdir(scenario_dir) if i.endswith("npz")]
    outdir = os.path.join(os.path.split(scenario_dir)[0], "summary_statistics")
    os.makedirs(outdir, exist_ok=True)
    if nrep != "all":
        npzfiles = npzfiles[:nrep]
    for npzfile in npzfiles:
        allel_count = -1
        haplotype = -1
        try:
            sim_id = os.path.splitext(os.path.basename(npzfile))[0]
            snp_mat, pos_vec = read_ms_compressed(os.path.join(scenario_dir, npzfile))
            #snp_mat = utils.transform_to_min_major(snp_mat)
            #snp_mat, pos_vec = utils.remove_maf(snp_mat, pos_vec, maf)
            # convert in total size
            if pos_vec.max() <= 1:
                pos_vec = (pos_vec * size_chr).round().astype(int)
            n_indiv = snp_mat.shape[0]
            haplotype = allel.HaplotypeArray(snp_mat.T)
            allel_count = haplotype.count_alleles()
            derived_allel_count = allel_count.T[1]
            model, scenario, replicat, run_id = split_simid(sim_id)
        except Exception as e:
            logging.error("While loading {}\n>>> Error: {}".format(npzfile, e))
        try:
            df_sfs = sfs(haplotype, allel_count)
            all_sfs = pd.concat([all_sfs, df_sfs])
        except Exception as e:
            logging.error("While computing SFS for {}\n>>> Error: {}".format(sim_id, e))
        try:
            ld = LD(haplotype, pos_vec, size_chr=size_chr, circular=circular)
            ld["sim_id"] = sim_id
            ld["scenario"] = scenario
            ld["run_id"] = run_id
            ld["label"] = label
            all_ld = pd.concat([all_ld, ld])

        except Exception as e:
            logging.error("While computing LD for {}\n>>> Error: {}".format(sim_id, e))
    all_sfs2 = all_sfs.groupby("N_indiv").mean()
    all_sfs2["i_xi_norm"] = all_sfs2.i_xi / all_sfs2.i_xi.mean()
    all_sfs2["freq_indiv"] = all_sfs2.index / n_indiv
    all_sfs2["i_xi_sem_norm"] = all_sfs.groupby("N_indiv").i_xi.sem() / all_sfs2.i_xi.mean()
    all_sfs2["sim_id"] = sim_id
    all_sfs2["scenario"] = scenario
    all_sfs2["label"] = label
    all_sfs2.reset_index(inplace=True)
    writing_mode = "w" if overwrite else "a"
    if one_file:
        all_ld = all_ld.drop(columns=['sem_r2', 'run_id', 'label', 'sim_id'])
        ld = pd.DataFrame()
        for g in all_ld.groupby("scenario"):
            ld = pd.concat([ld, g[1].groupby("dist_group").mean()])
        all_ld = ld
        del all_sfs2['sim_id']
        df_sfs = all_sfs2.set_index('N_indiv')
        df_sfs = df_sfs.drop(columns=['label'])
        df_sfs_out = df_sfs.loc[df_sfs['scenario']==scenario]
        df_sfs_out = df_sfs_out.drop(columns=['scenario'])
        df_sfs_out = df_sfs_out.stack(dropna=False)
        df_sfs_out.index = df_sfs_out.index.map('{0[1]}_{0[0]}'.format)
        df_sfs_out = df_sfs_out.to_frame().T
        df_sfs_out = df_sfs_out.set_index([[scenario]])
        
        df_ld_out = all_ld.loc[np.array(all_ld['scenario']==scenario)]
        df_ld_out = df_ld_out.drop(columns=['scenario'])
        df_ld_out = df_ld_out.stack(dropna=False)
        df_ld_out.index = df_ld_out.index.map('{0[1]}_{0[0]}'.format)
        df_ld_out = df_ld_out.to_frame().T
        df_ld_out = df_ld_out.set_index([[scenario]])
        df = pd.merge(df_sfs_out, df_ld_out, left_index=True, right_index=True)
        with open(os.path.join(outdir, name_id + "_summary_statistics_no_maf_030819.csv"), 'a') as filename:
            df.to_csv(filename, mode=writing_mode, header=filename.tell()==0,  index_label="scenario")

    else:
        with open(os.path.join(outdir, name_id + ".sfs"), writing_mode) as sfsfile:
            all_sfs2.to_csv(sfsfile,
                            sep="\t",
                            index=False,
                            header=False if (sfsfile.tell()==0 and not overwrite) else True)

        all_ld2 = all_ld.groupby("dist_group").mean()
        all_ld2["sim_id"] = sim_id
        all_ld2["scenario"] = scenario
        all_ld2["label"] = label
        all_ld2.reset_index(inplace=True)
        with open(os.path.join(outdir, name_id + ".ld"), writing_mode) as ldfile:
            all_ld2.to_csv(ldfile,
                           sep="\t",
                           index=False,
                           header=False if (ldfile.tell()==0 and not overwrite) else True)
    #
    # window = 100
    # try:
    #     taj = tajimasD(haplotype, pos_vec, window=window)
    # except Exception as e:
    #     logging.error("While doing Tajimas'D for {}\n>>> Error: {}".format(sim_id, e))
    #     taj = pd.DataFrame()
    # try:
    #     ihs_ser = ihs(haplotype, pos_vec, window=window)
    # except Exception as e:
    #     logging.error("While doing IHS for {}\n>>> Error: {}".format(sim_id, e))
    #     ihs_ser = pd.Series()
    # try:
    #     nsl_ser = nsl(haplotype, pos_vec, window=window)
    # except Exception as e:
    #     logging.error("While doing NSL for {}\n>>> Error: {}".format(sim_id, e))
    #     nsl_ser = pd.Series()
    #
    # df_sel = pd.concat([taj, ihs_ser, nsl_ser], axis=1)
    # df_sel.index.name = "position_percent"
    # df_sel.reset_index(inplace=True)
    # df_sel["sim_id"] = sim_id
    # df_sel["scenario"] = scenario
    # df_sel["run_id"] = run_id
    # df_sel["label"] = label
    # df_sel.to_csv(os.path.join(name_id + ".sel"), sep="\t", index=False, mode="a", na_rep="NaN", header=False)

def worker_do_sum_stats_geno(param):
    do_sum_stats_geno(**param)
    
def do_sum_stats_geno(scenario_dir, name_id, size_chr=2e6, circular=True, label="", nrep="all", one_file=False, overwrite=False):
    all_sfs = pd.DataFrame()
    all_ld = pd.DataFrame()
    npzfiles = [i for i in os.listdir(scenario_dir) if i.endswith("npz")]
    outdir = os.path.join(os.path.split(scenario_dir)[0], "summary_statistics")
    os.makedirs(outdir, exist_ok=True)
    if nrep != "all":
        npzfiles = npzfiles[:nrep]
    for npzfile in npzfiles:
        allel_count = -1
        geno = -1
        try:
            sim_id = os.path.splitext(os.path.basename(npzfile))[0]
            snp_mat, pos_vec = read_ms_compressed(os.path.join(scenario_dir, npzfile))
            n_indiv = snp_mat.shape[0]
            cp = snp_mat.T.copy()
            cp[cp==1] = 0
            genotype = np.moveaxis(np.array([cp, snp_mat.T]), 0, -1)
            genotype[genotype==2] = 1
            genotype = allel.GenotypeArray(genotype)
            allel_count = genotype.count_alleles()
            derived_allel_count = allel_count.T[1]
            model, scenario, replicat, run_id = split_simid(sim_id)
        except Exception as e:
            logging.error("While loading {}\n>>> Error: {}".format(npzfile, e))
        try:
            df_sfs = sfs(genotype, allel_count, nindiv=26)
            all_sfs = pd.concat([all_sfs, df_sfs])
        except Exception as e:
            logging.error("While computing SFS for {}\n>>> Error: {}".format(sim_id, e))
        try:
            
            ld = LD(snp_mat.T, pos_vec, size_chr=size_chr, circular=circular)
            ld["sim_id"] = sim_id
            ld["scenario"] = scenario
            ld["run_id"] = run_id
            ld["label"] = label
            all_ld = pd.concat([all_ld, ld])

        except Exception as e:
            logging.error("While computing LD for {}\n>>> Error: {}".format(sim_id, e))
    all_sfs2 = all_sfs.groupby("N_indiv").mean()
    all_sfs2["i_xi_norm"] = all_sfs2.i_xi / all_sfs2.i_xi.mean()
    all_sfs2["freq_indiv"] = all_sfs2.index / n_indiv
    all_sfs2["i_xi_sem_norm"] = all_sfs.groupby("N_indiv").i_xi.sem() / all_sfs2.i_xi.mean()
    all_sfs2["sim_id"] = sim_id
    all_sfs2["scenario"] = scenario
    all_sfs2["label"] = label
    all_sfs2.reset_index(inplace=True)
    writing_mode = "w" if overwrite else "a"
    if one_file:
        all_ld = all_ld.drop(columns=['sem_r2', 'run_id', 'label', 'sim_id'])
        ld = pd.DataFrame()
        for g in all_ld.groupby("scenario"):
            ld = pd.concat([ld, g[1].groupby("dist_group").mean()])
        all_ld = ld
        del all_sfs2['sim_id']
        df_sfs = all_sfs2.set_index('N_indiv')
        df_sfs = df_sfs.drop(columns=['label'])
        df_sfs_out = df_sfs.loc[df_sfs['scenario']==scenario]
        df_sfs_out = df_sfs_out.drop(columns=['scenario'])
        df_sfs_out = df_sfs_out.stack(dropna=False)
        df_sfs_out.index = df_sfs_out.index.map('{0[1]}_{0[0]}'.format)
        df_sfs_out = df_sfs_out.to_frame().T
        df_sfs_out = df_sfs_out.set_index([[scenario]])
        
        df_ld_out = all_ld.loc[np.array(all_ld['scenario']==scenario)]
        df_ld_out = df_ld_out.drop(columns=['scenario'])
        df_ld_out = df_ld_out.stack(dropna=False)
        df_ld_out.index = df_ld_out.index.map('{0[1]}_{0[0]}'.format)
        df_ld_out = df_ld_out.to_frame().T
        df_ld_out = df_ld_out.set_index([[scenario]])
        df = pd.merge(df_sfs_out, df_ld_out, left_index=True, right_index=True)
        with open(os.path.join(outdir, name_id + "_sample_summary_statistics.csv"), 'a') as filename:
            df.to_csv(filename, mode=writing_mode, header=filename.tell()==0,  index_label="scenario")

    else:
        with open(os.path.join(outdir, name_id + ".sfs"), writing_mode) as sfsfile:
            all_sfs2.to_csv(sfsfile,
                            sep="\t",
                            index=False,
                            header=False if (sfsfile.tell()==0 and not overwrite) else True)

        all_ld2 = all_ld.groupby("dist_group").mean()
        all_ld2["sim_id"] = sim_id
        all_ld2["scenario"] = scenario
        all_ld2["label"] = label
        all_ld2.reset_index(inplace=True)
        with open(os.path.join(outdir, name_id + ".ld"), writing_mode) as ldfile:
            all_ld2.to_csv(ldfile,
                           sep="\t",
                           index=False,
                           header=False if (ldfile.tell()==0 and not overwrite) else True)
    
def load_sum_stats(name_id, label=""):
    """Load data from name_id/name_id.{sfs|ld|sel} and return the 3 df"""

    df_sfs = pd.read_table(os.path.join(name_id+ ".sfs"))
    df_ld = pd.read_table(os.path.join(name_id+ ".ld"))

    # df_sel = pd.read_table(os.path.join(name_id+ ".sel"),
    #                        header=None,
    #                        names=["position_percent", "TajD", "IHS", "NSL", "sim_id", "scenario", "run_id", "label"])
    if label != "":
        df_sfs["label"] = label
        df_ld["label"] = label

    return df_sfs, df_ld#, df_sel

def plot_fill(x, y, color, ax, step=1, label=None):
    """
    plot the mean as a line and the standard error to the mean as a shade.

    Parameters
    ----------
    x : array
        x-coordinate.
    y : groupby object
        distribution of y-coordinate, from which the mean and sem will be computed.
    color : str
        A color for the mean and the fill area around.
    ax : Axes object
        Where to plot.
    step : int
        to sample the x and y values every `step` values.
    label : str
        Describe the (x, y) curve.

    """
    x = x[::step]
    y_mean, y_sem = y.mean()[::step], y.sem()[::step]
    ax.plot(x, y_mean, color=color, label=label)
    ax.fill_between(x, y_mean + y_sem, y_mean - y_sem, color=color, lw=0, alpha=0.5)


def relative_position(positions, size_chr=0):
    """ Given a numpy array with absolute positions,
        return a numpy array with the relative positions.
        i.e.:
            pos_rel[i] = position[i+1] - position[i]
        if the chromosome is circular:
            pos_rel[0] = (position[-1] - position[0])%size_chr

    Parameters
    ----------
    positions : numpy array
        Absolute positions of SNPs
    size_chr : int
        if 0, the first relative position will be , otherwise it il

    Returns
    -------
    pos_rel: numpy array
        Relative positions of SNPs
    """

    if size_chr == 0:
        return np.ediff1d(positions, to_begin=positions[0])
    else:
        return np.ediff1d(positions, to_begin=(positions[0] - positions[-1]) % size_chr)

def plot_sfs(df_sfs, by="scenario", window=1, ax=None, legend=True):
    """
    Function to plot sfs

    Parameters
    ----------
    df_sfs : dataframe
        Table generated by the sfs() function
    by : str
        Column on which to group the data.
        By default, `scenario`, but it could be `label`, or a another created columns
    step : int
        If you want to subsample your sfs, and use every `step` values to plot
        instead of all.
    ax : matplotlib Axes
        to plot it in a subplot.
    legend: bool
        Whether to plot the legend

    Returns
    -------
    None
    """

    uniq_ID = df_sfs[by].sort_values().unique()
    nsamp = df_sfs.N_indiv.max()
    if len(uniq_ID) > 1:
        dic_color = {j:plt.cm.viridis(int(i*255/(len(uniq_ID)-1))) for i,j in enumerate(uniq_ID)}
    else:
        dic_color = {uniq_ID[0] : plt.cm.viridis(128)}

    if ax == None:
        fig, ax = plt.subplots(1,1)

    for g in df_sfs.groupby(by):
        color = dic_color[g[0]]
        if window > 1:
            g[1].groupby("N_indiv")[["freq_indiv", "i_xi_norm"
             ]].mean().rolling(window).mean().dropna().plot(x="freq_indiv",
                                            y="i_xi_norm",
                                            yerr=g[1].groupby("N_indiv")[["freq_indiv", "i_xi_sem_norm"
                                                 ]].mean().rolling(window).i_xi_sem_norm.mean(),
                                            ax = ax,
                                            label=g[0],
                                            color=color)

        else:
            # It groupby on N_indiv even though it supposed to have 1 value
            # but it depends on the outer group defined by "by=". If it is
            # something else than "scenario", it can have more than 1 value for
            # a given N_indiv category.
            g[1].groupby("N_indiv")[["freq_indiv", "i_xi_norm"
             ]].mean().plot(x="freq_indiv",
                            y="i_xi_norm",
                            yerr=g[1].groupby("N_indiv")[["freq_indiv",
                                                          "i_xi_sem_norm"
                                                        ]].mean(),
                            ax=ax,
                            color=color,
                            label=g[0])

    ax.axhline(1/(nsamp),
                  color="0.5",
                  zorder=10,
                  linestyle="--")
    if legend:
        ax.legend(loc=6, bbox_to_anchor=(1, 0.5))
    else:
        ax.legend_.set_visible(False)

def plot_ld(df_ld, by="scenario", ax=None, legend=True):
    """Function to plot the Linkage Desiquilibrium.

    Parameters
    ----------
    df_ld : DataFrame
        Dataframe generated by the ld() function.
    by : str
        Column on which to group the data.
        By default, `scenario`, but it could be `label`, or a another created columns
    ax : Axes
        to plot in a subplot.
    legend: bool
        Whether to plot the legend

    Returns
    -------
    None
    """

    uniq_ID = df_ld[by].sort_values().unique()
    if len(uniq_ID) > 1:
        dic_color = {j:plt.cm.viridis(int(i*255/(len(uniq_ID)-1))) for i,j in enumerate(uniq_ID)}
    else:
        dic_color = {uniq_ID[0]: plt.cm.viridis(128)}

    if ax == None:
        fig, ax = plt.subplots(1,1)

    for g in df_ld.groupby(by):
        color = dic_color[g[0]]
        g[1].groupby("dist_group").mean().plot(x="mean_dist",
                                               y="mean_r2",
                                               yerr=g[1].groupby("dist_group").mean_r2.sem(),
                                               kind="scatter",
                                               label=g[0],
                                               ax=ax,
                                               color=color,
                                               legend=True)
    ax.set_xscale("log")
    ax.set_xlim(10,
                10**round(np.log10(df_ld.mean_dist.max())))
    if legend:
        ax.legend(loc=6, bbox_to_anchor=(1, 0.5))
    else:
        ax.legend_.set_visible(False)

def change_range(x, min_val, max_val):
    """x is in [0, 1]. Return x in [min_val, max_val]"""

    nx = x * (max_val - min_val)  + min_val
    return nx

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--datapath", default=None, type=str)
    parser.add_argument("--workdir", default=None, type=str)
    args = parser.parse_args()
    sys.path.append(args.workdir)
    os.chdir(args.workdir)
    datapath = args.datapath
    params = []
    dir_list = [i for i in os.listdir(datapath) if i[:8] == 'scenario']
    for scenario in dir_list:
        dir_scenario = os.path.join(datapath, scenario)
        params.append({"scenario_dir": dir_scenario, "name_id":"cattle", 'circular':False, 'one_file':True})     
        #params.append({"scenario_dir": dir_scenario, "name_id":"cattle_genotype_400SNP", 'circular':False, 'one_file':True})     
    cores = multiprocessing.cpu_count()
    with multiprocessing.Pool(cores) as pool:
        pool.map(worker_do_sum_stats, params)
        #pool.map(worker_do_sum_stats_geno, params)
        pool.close()
        pool.join()
    #df = reduce(lambda x, y: pd.merge(x, y, how='outer'), res)
    #with open(os.path.join(os.path.split(dir_scenar)[0], "summary_statistics", model + "_2_summary_statistics.csv"), 'a') as filename:
    #    df.to_csv(filename, mode='a', header=filename.tell()==0,  index_label="scenario")
