#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import os
import logging
import sys
import multiprocessing
import numpy as np
import pandas as pd
from functools import partial
import utils


class TrainingParam:

    def __init__(self, training_param_path=None, num_validation_scenarios=600, num_training_scenario=5428,
                 scenario_param_path='/home/tau/thsanche/data/cattle_genotype_400SNP/cattle_demo_parameters.csv',
                 simulation_param_path='/home/tau/thsanche/data/cattle_genotype_400SNP/cattle_simu_parameters.json',
                 run_path='/home/tau/thsanche/run', param_2_learn=['population_size_' + str(i) for i in range(21)], 
                 param_2_log=None, run_name='run_0',
                 SNP_min=400, use_cuda=True, cuda_device=None,
                 num_epoch=2, learning_rate=0.001, weight_decay=0, batch_size=100,
                 evaluation_interval=100000, network_name='SPIDNA2',
                 seed=2, loader_num_workers=20, num_block=7, num_feature=50,
                 sample_min=50, train_mean=None, train_std=None, maf=0, transform_allel_min_major=False, 
                 start_from_last_checkpoint=False, alpha=None, **kwargs):
        if training_param_path is None:
            for key, value in locals().items():
                if key != 'self' and key != 'training_param_path' and key !='kwargs':
                    setattr(self, key, value)
            for key, value in kwargs.items():
                setattr(self, key, value)
        else:
            self.load(training_param_path)

    def save(self, training_param_path):
        logging.info(f'Saving training parameters at: {training_param_path}')
        utils.save_dict_in_json(training_param_path, vars(self))

    def load(self, training_param_path):
        training_param_dict = utils.load_dict_from_json(training_param_path)
        for key, value in training_param_dict.items():
            setattr(self, key, value)

    def preprocess(self, overwrite):
        simulation_param = utils.load_dict_from_json(self.simulation_param_path)
        np.random.seed(self.seed)
        if overwrite or not os.path.isdir(os.path.join(self.run_path, simulation_param['model_name'], self.run_name)):
            num_run = str(int(self.run_name.split('_')[-1]))
        else:
            run_dir_list = [i for i in os.listdir(os.path.join(self.run_path, simulation_param['model_name'])) if i[:3] == 'run']
            num_run = int(self.run_name.split('_')[-1])
            num_run = str(max([int(dir_name.split('_')[-1]) for dir_name in run_dir_list] + [num_run]) + 1)
        self.run_name = f'run_{num_run}'
        try:
            scenario_param = pd.read_csv(self.scenario_param_path,
                                          index_col='scenario_idx', # should have index as first column
                                          sep=None, # autodetect separator
                                          engine='python') # to use sep=None
        except ValueError as e:
            if 'scenario_idx' in str(e):
                logging.error(f'A columns scenario_idx must exist in your table {self.scenario_param_path}.')
                # TODO if no index in the table, create an option --create_index
            else:
                logging.error(e)
            sys.exit(0)
        if self.param_2_learn is None:
            self.param_2_learn = [param for param in list(scenario_param) if 'size' in param or 'time' in param]
            logging.info(f'No parameters to learn provided, using parameters with "size" or "time" in their name: {self.param_2_learn}')
        scenario_param =  scenario_param.sort_index().iloc[0:self.num_training_scenario +
                                                           self.num_validation_scenarios]
        logging.info(f'Removing scenario without full or with less than {self.SNP_min} SNPs...')
        # new function with partial application of the given arguments and keywords.
        partial_check_scenario = partial(check_scenario,
                                         simulation_param['simulation_data_path'],
                                         simulation_param['model_name'],
                                         self.SNP_min,
                                         self.maf)

        cores = multiprocessing.cpu_count()
        with multiprocessing.Pool(cores) as pool:
            to_keep = np.array(pool.map(partial_check_scenario, scenario_param.iterrows()))
        to_keep = to_keep[to_keep[:, 0].argsort()] # sort to_keep by scen_idx values
        logging.info(f'{sum(to_keep[:, 1])} scenarios over {len(scenario_param)} have been kept.')
        logging.info('Splitting scenarios between training and validation set.')
        scenario_param = scenario_param.loc[to_keep[:, 1]==1]
        val = np.tile(False, self.num_validation_scenarios)
        if scenario_param.shape[0] - self.num_validation_scenarios > 0:
            train = np.tile(True, scenario_param.shape[0] - self.num_validation_scenarios)
        else:
            logging.error('num_validation_scenarios is greater than the number of scenario.')
            raise(ValueError)
        train_set = np.concatenate((val, train))
        np.random.shuffle(train_set)
        scenario_param['training_set'] = train_set
        if self.param_2_log is None:
            self.param_2_log = [param for param in list(scenario_param) if 'size' in param]
            logging.info(f'No parameters to log transform have been provided, log-transforming parameters with "size" in their name:{self.param_2_log}')
        if self.param_2_log != []:
            logging.info('Log-transform demographic parameters.')
            scenario_param[self.param_2_log] = scenario_param[self.param_2_log].apply(np.log)

        logging.info('Standardize demographic parameters.')
        train_param = scenario_param.loc[scenario_param['training_set']]
        train_param = train_param.drop('training_set', 1)
        train_mean = train_param[self.param_2_learn].mean()
        train_std = train_param[self.param_2_learn].std()
        scenario_param[self.param_2_learn] = (
            (scenario_param[self.param_2_learn] - train_mean) / train_std)

        self.train_mean = train_mean.to_dict()
        self.train_std = train_std.to_dict()
        # Save training parameters and preprocessed scenario parameters
        current_run_path = os.path.join(self.run_path,
                                        simulation_param['model_name'],
                                        self.run_name)
        os.makedirs(current_run_path, exist_ok=True)
        scenario_param_filename = f'{simulation_param["model_name"]}_{self.run_name}_preprocessed_param.csv'
        logging.info(f'Saving preprocessed scenario parameters at: {os.path.join(current_run_path, scenario_param_filename)}')
        scenario_param_path = os.path.join(current_run_path, scenario_param_filename)
        scenario_param.to_csv(scenario_param_path, index_label='scenario_idx')
        self.preprocessed_scenario_param_path = scenario_param_path
        training_param_path = os.path.join(current_run_path, f'{simulation_param["model_name"]}_{self.run_name}_training_param.json')
        
        self.save(training_param_path)
        return training_param_path

def check_scenario(simulation_data_path, model_name, SNP_min, maf, scenario_row):
    pass_check = True
    scen_idx, scenario = scenario_row
    for j in range(int(scenario['num_replicates'])):
        try:
            npz_path = utils.generate_filename((scen_idx, j),
                                               model_name,
                                               return_abs_path=True)
            with np.load(os.path.join(simulation_data_path, npz_path)) as data:
                if maf != 0:
                    snp, pos = utils.remove_maf_folded(data['SNP'], None, maf)
                    if snp.shape[1] < SNP_min:
                        pass_check = False
                        logging.warning(f'{npz_path} not kept because it had {snp.shape[1]} SNPs (< {SNP_min} SNPs)')
                        break
                else:
                    if data['POS'].shape[0] < SNP_min:
                        pass_check = False
                        logging.warning(f'{npz_path} not kept because it had {data["POS"].shape[0]} SNPs (< {SNP_min} SNPs)')
                        break
                        # TODO ADD CHECK for relative positions
                        # TODO with warning + asking for confirmation if conversion+overwrite (before multiprocessing)

        except FileNotFoundError:
            pass_check = False
            logging.warning(f'{os.path.join(simulation_data_path, npz_path)} not found. Skip whole scenario.')
            break
        except OSError as e:
            pass_check = False
            logging.warning(f'OSError: {e}, on file {os.path.join(simulation_data_path, npz_path)}. Skip whole scenario')
            break
    return scen_idx, pass_check

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--training_param_path', default=None, type=str)
    parser.add_argument('--loglevel',
                         default='INFO',
                         action='store',
                         type=str,
                         metavar='INFO',
                         help='Amount of log info: DEBUG, [INFO], WARNING, ERROR, CRITICAL.')
    parser.add_argument('--overwrite',
                        help='Overwrite run (otherwise, create a new run).',
                        action='store_true')
    parser.add_argument('--start_preprocessing',
                        help='Start the preprocessing and create new training parameters if no training_param_path provided.',
                        action='store_true')
    args = parser.parse_args()
    logging.basicConfig(stream=sys.stdout,
                        #filename=os.path.join(out_dir, 'output.log'),
                        level=logging.getLevelName(args.loglevel),
                        format='%(asctime)s; %(levelname)s; %(message)s',
                        datefmt='%d/%m/%Y %H:%M:%S')

    training_param = TrainingParam(training_param_path=args.training_param_path)
    if args.training_param_path is None:
        training_param.save('training_param.json')
    elif args.overwrite:
        training_param.save(training_param_path=args.training_param_path)
    if args.start_preprocessing:
        training_param.preprocess(overwrite=args.overwrite)
