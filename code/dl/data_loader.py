#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import pandas as pd
import numpy as np
import torch
from torch.utils.data import Dataset, DataLoader, sampler
import utils
import logging
import numpy as np


class DNADataset(Dataset):
    def __init__(self, simulation_data_path, indices, SNP_min, scenario_param_preprocessed, maf, transform_allel_min_major, sample_min):
        self.simulation_data_path = simulation_data_path
        self.indices = indices
        self.set_size = len(indices)
        self.SNP_min = SNP_min
        self.scenario_param_preprocessed = scenario_param_preprocessed
        self.maf = maf
        self.transform_allel_min_major = transform_allel_min_major
        self.sample_min = sample_min

    def __getitem__(self, index):

        scenario_idx, npz_path = self.indices[index]
        scenario = self.scenario_param_preprocessed.loc[scenario_idx]

        with np.load(os.path.join(self.simulation_data_path, npz_path)) as data_npz:
            #snp = torch.Tensor(data_npz['SNP'][:self.sample_min, :self.SNP_min])
            #snp = utils.transform_to_min_major(snp)
            #input_val = torch.cat((torch.Tensor(data_npz['POS'][:self.SNP_min]).unsqueeze(0),
            #                      snp))
            
            #pos = (torch.Tensor(data_npz['POS'][:self.SNP_min]).unsqueeze(0) - 1172.3192) / 1700.1180 #####
            
            #pos = torch.Tensor(data_npz['POS'][:self.SNP_min]).unsqueeze(0)
            #pos[pos == 0] = 1
            #pos = torch.log(pos)
            #pos = (pos - 6.1872) / 1.5064 #####
            #input_val = torch.cat((pos,
            #                       torch.Tensor(data_npz['SNP'][:self.sample_min, :self.SNP_min])))
            snp = data_npz['SNP']
            pos = data_npz['POS']
            if self.transform_allel_min_major:
                snp = utils.transform_to_min_major(snp)
            snp, pos = utils.remove_maf_folded(snp, pos, self.maf)
            
            input_val = torch.cat((torch.Tensor(pos[:self.SNP_min].astype('float32')).unsqueeze(0),
                                   torch.Tensor(snp[:self.sample_min, :self.SNP_min].astype('float32'))))

        target = torch.Tensor(scenario.values)
        #logging.debug(f" shape: {input_val.shape}, path: {npz_path}")
        return scenario_idx, input_val, target

    def __len__(self):
        return self.set_size


def generate_loaders(model_name, param_2_learn, preprocessed_scenario_param_path,
                     simulation_data_path, batch_size, use_cuda, loader_num_workers,
                     SNP_min, maf, transform_allel_min_major, sample_min, **kwargs):

    scenario_param_preprocessed = pd.read_csv(preprocessed_scenario_param_path, index_col="scenario_idx").sort_index()
    indices = {}
    training_indices = set()
    validation_indices = set()
    i = 0
    for scenario in scenario_param_preprocessed.itertuples():
        #if (i % 500) == 0:
            #logging.debug(scenario)
        for k in range(scenario.num_replicates):
            indices[i] = (scenario.Index,
                          utils.generate_filename((scenario.Index, k),
                                                  model_name,
                                                  return_abs_path=True))
            if scenario.training_set:
                training_indices.add(i)
            else:
                validation_indices.add(i)
            i += 1

    logging.debug(f'They are {len(validation_indices)} data in the validation set and {len(training_indices)} in the training set.')

    train_sampler = sampler.SubsetRandomSampler(list(training_indices))
    validation_sampler = sampler.SubsetRandomSampler(list(validation_indices))

    scenario_param_preprocessed = scenario_param_preprocessed[param_2_learn]

    dataset = DNADataset(simulation_data_path, indices, SNP_min, scenario_param_preprocessed, maf, transform_allel_min_major, sample_min)
    train_loader = DataLoader(dataset=dataset,
                              batch_size=batch_size,
                              sampler=train_sampler,
                              num_workers=loader_num_workers,
                              pin_memory=use_cuda)

    validation_loader = DataLoader(dataset=dataset,
                                   batch_size=batch_size,
                                   sampler=validation_sampler,
                                   num_workers=loader_num_workers,
                                   pin_memory=use_cuda)


    return train_loader, validation_loader
