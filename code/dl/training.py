#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import datetime
import argparse
import time
import numpy as np
import net as net_module
import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim
import data_loader
import utils
import logging
import sys
import os
from collections import OrderedDict

start_time = time.time()


def validation_loop(validation_loader, net, device, num_block, network_name):
    #utils.debug_gpu('Before validation:')
    net.eval() # important for BatchNorm (or dropout)
    all_targets = torch.Tensor()
    all_outputs = torch.Tensor()
    with torch.no_grad(): # does not compute gradients, useful in training mode
        # Compute all outputs for validation dataset
        for j, (scen_idx, inputs_val, targets_val) in enumerate(validation_loader):
            inputs_val, targets_val = inputs_val.to(device), targets_val.to(device)
            outputs = net(inputs_val)
            all_outputs = torch.cat((all_outputs, outputs.cpu()), 0)
            all_targets = torch.cat((all_targets, targets_val.cpu()), 0)
        # error on entire validation dataset
        all_targets = all_targets.numpy()
        all_outputs = all_outputs.numpy()
        if network_name=='SPIDNA2_loss':
            all_outputs = np.mean(all_outputs, 1)
        validation_error = ((all_outputs - all_targets)**2)
        validation_error = np.mean(validation_error, 0)
        # return a single target per scenario
        unique_all_targets = np.unique(all_targets, axis=0)
        # for each scenario (same targets), compute the mean of the predicted parameters over replicates
        mean_outputs_per_scenario = np.array([np.mean(all_outputs[(all_targets == unique_all_targets[m]).all(axis=1)], 0) for m in range(unique_all_targets.shape[0])])
        # compute error per scenario
        validation_error_grouped = (mean_outputs_per_scenario - unique_all_targets)**2
        validation_error_grouped = np.mean(validation_error_grouped, 0)
    return validation_error, validation_error_grouped

def training_loop(net, train_loader, validation_loader, device, num_output,
             model_name, error_log, num_epoch, evaluation_interval, run_path,
             run_name, SNP_min, batch_size, criterion, optimizer, train_mean,
             train_std, param_2_learn, param_2_log, network_name, num_block, budget=None, ** kwargs):
    logging.info('Start training')
    net.train() # important for BatchNorm (or dropout)
    best_validation_error_grouped = np.ones(num_output)
    training_step = 0 #
    for e in range(num_epoch):
        logging.debug(f'Epoch: {e}')
        errors_train = []
        for i, (scen_idx, inputs_train, targets_train) in enumerate(train_loader):
            inputs_train, targets_train = inputs_train.to(device), targets_train.to(device)
            outputs_train = net(inputs_train)
            #training_step = inputs_train.shape[0] * i + e * len(train_loader)
            training_step += inputs_train.shape[0]
            train_loss = criterion(outputs_train, targets_train)
            errors_train.append(torch.mean(train_loss, 0, keepdim=True).detach().cpu().numpy())
            train_loss = torch.mean(train_loss)
            if (inputs_train.shape[0] * i) % evaluation_interval < inputs_train.shape[0] or (budget is not None and training_step > budget):
                logging.debug('Evaluation step')
                all_errors = {}
                errors_train = np.concatenate(errors_train)
                all_errors['train_error'] = np.mean(errors_train, 0)
                errors_train = []
                all_errors['validation_error'], all_errors['validation_error_grouped'] = validation_loop(validation_loader=validation_loader, net=net, device=device, num_block=num_block, network_name=network_name)
                logging.debug(f"--- validation_error: {all_errors['validation_error']} -- validation_error_grouped: {all_errors['validation_error_grouped']} ---")
                for key, df in error_log.items():
                    df.loc[len(df)] = [training_step] + list(all_errors[key])
                    path = os.path.join(run_path, model_name, run_name, f'{model_name}_{run_name}_{key}.csv')
                    df.loc[[len(df)-1]].to_csv(path,  mode='a', header=False)

                if np.mean(all_errors['validation_error_grouped']) < np.mean(best_validation_error_grouped):
                    best_validation_error_grouped = all_errors['validation_error_grouped']
                    net_path = os.path.join(run_path, model_name, run_name,
                                            f"{model_name}_{run_name}_net.pth")
                    torch.save(net.state_dict(), net_path)
                net.train()
            if SNP_min is None:
                train_loss = train_loss / batch_size
                train_loss.backward()
                if i % batch_size == 0:
                    optimizer.step() # update the weights
                    optimizer.zero_grad() # clear the gradients of all optimized tensors
            else:
                train_loss.backward()
                optimizer.step()
                optimizer.zero_grad()
            del train_loss
            if budget is not None and training_step > budget:
                logging.debug(f'Budget ({budget}) reached.')
                break
        net_path = os.path.join(run_path, model_name, run_name,
                            f"{model_name}_{run_name}_last_epoch_net.pth")
        torch.save(net.state_dict(), net_path)
        if budget is not None and training_step > budget:
            break
    return best_validation_error_grouped

def start_training(training_param_path, budget=None):
    torch.cuda.synchronize()
    training_param = utils.load_dict_from_json(training_param_path)
    np.random.seed(training_param['seed'])
    torch.manual_seed(training_param['seed'])
    simul_param = utils.load_dict_from_json(os.path.join(training_param['simulation_param_path']))
    num_output = len(training_param['param_2_learn'])

    # Set specified GPU
    device = torch.device('cuda' if torch.cuda.is_available and training_param['use_cuda'] else 'cpu')
    if device.type == 'cuda' and training_param['cuda_device'] is not None:
        torch.cuda.set_device(training_param['cuda_device'])

    date = datetime.datetime.fromtimestamp(time.time()).strftime('_%Y-%m-%d_%H-%M-%S')
    net = getattr(net_module, training_param['network_name'])
    if training_param['network_name'] == 'SPIDNA2_in_alpha' or training_param['network_name'] == 'SPIDNA2_in_fc_alpha':
        net_param = {'num_output': num_output,
                     'num_block': training_param['num_block'],
                     'num_feature': training_param['num_feature'],
                     'device': device,
                     'alpha': training_param['alpha']}
    elif training_param['network_name'][:7] == 'SPIDNA2':
        net_param = {'num_output': num_output,
                     'num_block': training_param['num_block'],
                     'num_feature': training_param['num_feature'],
                     'device':device}
    else:
        net_param = {'num_SNP': training_param['SNP_min'],
                     'num_sample': training_param['sample_min'],
                     'num_output': num_output}
    net = net(**net_param).to(device)

    # Count model parameters
    num_parameter = utils.count_parameters(net)

    # Setup loss and optimizer
    criterion = nn.MSELoss(reduction='none').to(device)
    optimizer = optim.Adam(net.parameters(), lr=training_param['learning_rate'], weight_decay=training_param['weight_decay'])
    
    if training_param['start_from_last_checkpoint'] == True:
        state_dict = torch.load(os.path.join(training_param['run_path'], simul_param['model_name'], training_param['run_name'], f'cattle_{training_param["run_name"]}_last_epoch_net.pth'))
        # create new OrderedDict that does not contain `module.`
        new_state_dict = OrderedDict()
        for k, v in state_dict.items():
            name = k[7:] # remove `module.`
            new_state_dict[name] = v
        net.load_state_dict(new_state_dict)
        
    elif training_param['network_name'][:7] == 'SPIDNA2':
        net.apply(utils.init_weights)

    # Dispatch mini-batch on multiple GPUs
    if device.type == 'cuda':
        net = torch.nn.DataParallel(net, device_ids=range(torch.cuda.device_count()))

    # Load the dataset
    if training_param['SNP_min'] is None:
        tmp_batch_size = training_param['batch_size']
        training_param['batch_size'] = 1
    train_loader, validation_loader = data_loader.generate_loaders(**simul_param, **training_param)
    if training_param['SNP_min'] is None:
        training_param['batch_size'] = tmp_batch_size
                                            
    error_log = {}
    for key in ['train_error', 'validation_error', 'validation_error_grouped']:                                         
        error_log[key] = pd.DataFrame(columns=['training_step'] + training_param['param_2_learn'])
    for key, df in error_log.items():
        df.to_csv(os.path.join(training_param['run_path'],
                               simul_param['model_name'],
                               training_param['run_name'],
                               f'{simul_param["model_name"]}_{training_param["run_name"]}_{key}.csv'))

    best_errors_grouped_val = training_loop(net=net, train_loader=train_loader, validation_loader=validation_loader,
                                           device=device, num_output=num_output,
                                           model_name=simul_param['model_name'],
                                           error_log=error_log, criterion=criterion, optimizer=optimizer,
                                           budget=budget, **training_param)
    time_spent = time.time() - start_time
    logging.info('--- %s seconds ---' % (time_spent))
    file_path = os.path.join(training_param['run_path'],
                             simul_param['model_name'],
                             f"{simul_param['model_name']}_all_run_stats.csv")

    col = ['run_name', 'run_id', 'network_name', 'best_errors_grouped_val'] + training_param['param_2_learn'] + ['num_SNP', 'seed', 'num_epoch', 'learning_rate', 'weight_decay', 'num_block', 'evaluation_interval', 'use_cuda', 'batch_size', 'validation_set_size', 'training_set_size',
    'time_spent', 'num_parameter', 'budget', 'start_from_last_checkpoint', 'alpha']
    info = [training_param['run_name'], training_param['network_name'] + date, training_param['network_name'], np.mean(best_errors_grouped_val)] + list(best_errors_grouped_val) + [training_param['SNP_min'], training_param['seed'],
    training_param['num_epoch'], training_param['learning_rate'], training_param['weight_decay'], training_param['num_block'],
    training_param['evaluation_interval'], training_param['use_cuda'], training_param['batch_size'],
    training_param['num_validation_scenarios'], len(train_loader), time_spent, num_parameter, budget, training_param['start_from_last_checkpoint'], training_param['alpha']]
    run_info = pd.DataFrame([info], columns=col)
    os.makedirs(os.path.join(os.sep, *file_path.split('/')[:-1]), exist_ok=True)
    with open(file_path, 'a') as all_run_stats:
        run_info.to_csv(all_run_stats,
                        mode='a',
                        header=all_run_stats.tell()==0, # If the file is new, add header.
                        index=False)
    torch.cuda.empty_cache()
    return best_errors_grouped_val


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--training_param_path', default=None, type=str)
    parser.add_argument('--loglevel',
                         default='INFO',
                         action='store',
                         type=str,
                         metavar='INFO',
                         help='Amount of log info: DEBUG, [INFO], WARNING, ERROR, CRITICAL')
    args = parser.parse_args()
    logging.basicConfig(stream=sys.stdout,
                        #filename=os.path.join(out_dir, 'output.log'),
                        level=logging.getLevelName(args.loglevel),
                        format='%(asctime)s; %(levelname)s;  %(message)s',
                        datefmt='%d/%m/%Y %H:%M:%S')
    logging.info(start_training(args.training_param_path))
