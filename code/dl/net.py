#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import utils


class SPIDNA1(nn.Module):
    """SPIDNA neural network model.

    SPIDNA is a convolutionnal neural network that infers demographic parameters from a SNP matrix and its associated vector of position. The number of SNP is predefined and fixed.

    Arguments:
        num_SNP (int): number of SNPs in the aligment data
        num_output (int): number of demographic parameters to infer
    """

    def __init__(self, num_SNP, num_output, num_sample):
        super(SPIDNA1, self).__init__()
        self.num_sample = num_sample
        self.filters = [(2,2),(5,4),(3,8),(2,10),(20,1)]
        self.outputs_height = [20 * (20 - x + 1) + 5 for x in [2,5,3,2,20]]
        self.outputs_height_cumsum = [0]
        self.outputs_height_cumsum.extend([int(x) for x in np.cumsum(self.outputs_height)])

        self.conv1 = nn.ModuleList()
        self.batch_norm1 = nn.ModuleList()
        self.conv2 = nn.ModuleList()
        self.batch_norm2 = nn.ModuleList()
        self.conv3 = nn.ModuleList()
        self.batch_norm3 = nn.ModuleList()

        for f in range(len(self.filters)):
            self.conv1.append(nn.Conv1d(1, 5, self.filters[f][1]))
            self.batch_norm1.append(nn.BatchNorm1d(5))
            self.conv2.append(nn.Conv2d(1, 20, self.filters[f]))
            self.batch_norm2.append(nn.BatchNorm2d(20))
            self.conv3.append(nn.Conv2d(1, 50, (self.outputs_height[f], 1)))
            self.batch_norm3.append(nn.BatchNorm2d(50))

        self.conv4 = nn.Conv2d(1, 50, (250, 1))
        self.batch_norm4 = nn.BatchNorm2d(50)

        self.conv5 = nn.Conv2d(1, 50, (50, 3), stride = 3)
        self.batch_norm5 = nn.BatchNorm2d(50)

        self.conv6 = nn.Conv2d(1, 50, (50, 3), stride = 3)
        self.batch_norm6 = nn.BatchNorm2d(50)

        self.conv7 = nn.Conv2d(1, 50, (50, 3), stride = 3)
        self.batch_norm7 = nn.BatchNorm2d(50)

        self.conv8 = nn.Conv2d(1, 50, (50, 3), stride = 3)
        self.batch_norm8 = nn.BatchNorm2d(50)

        self.fc1 = nn.Linear(50 * ((num_SNP - 10 + 1) // 3 // 3 // 3 // 3), 50)
        self.batch_norm9 = nn.BatchNorm1d(50)

        self.fc2 = nn.Linear(50, num_output)

    def forward(self, x):
        batch_size = x.size(0)
        x = x.unsqueeze(1)
        x1 = x[:,:,0,:].contiguous()
        x2 = x[:,:,1:self.num_sample+1,:].contiguous()
        big_x = torch.Tensor()

        for f in range(len(self.filters)):
            x1_tmp = self.conv1[f](x1)
            x1_tmp = F.relu(self.batch_norm1[f](x1_tmp))
            x2_tmp = self.conv2[f](x2)
            x2_tmp = F.relu(self.batch_norm2[f](x2_tmp))
            x2_tmp = x2_tmp.view(batch_size, x2_tmp.size(1) * x2_tmp.size(2), x2_tmp.size(3))
            big_x = utils.cut_and_cat(big_x, torch.cat((x1_tmp, x2_tmp), 1))

        big_x = big_x.unsqueeze(1)

        big_x = torch.cat([F.relu(self.batch_norm3[f](self.conv3[f](big_x[:,:,self.outputs_height_cumsum[f]:self.outputs_height_cumsum[f+1],:]))).squeeze(2)
                            for f in range(len(self.filters))], 1).unsqueeze(1)

        big_x = self.conv4(big_x)
        big_x = F.relu(self.batch_norm4(big_x)).squeeze(2).unsqueeze(1)

        big_x = self.conv5(big_x)
        big_x = F.relu(self.batch_norm5(big_x)).squeeze(2).unsqueeze(1)

        big_x = self.conv6(big_x)
        big_x = F.relu(self.batch_norm6(big_x)).squeeze(2).unsqueeze(1)

        big_x = self.conv7(big_x)
        big_x = F.relu(self.batch_norm7(big_x)).squeeze(2).unsqueeze(1)

        big_x = self.conv8(big_x)
        big_x = F.relu(self.batch_norm8(big_x))

        big_x = big_x.view(big_x.size(0), -1)

        big_x = self.fc1(big_x)
        big_x = F.relu(self.batch_norm9(big_x))

        big_x = self.fc2(big_x)

        return big_x


class SPIDNA2Block(nn.Module):
    def __init__(self, num_output, num_feature):
        super(SPIDNA2Block, self).__init__()
        self.num_output = num_output
        self.phi = nn.Conv2d(num_feature * 2, num_feature, (1, 3))
        self.phi_bn = nn.BatchNorm2d(num_feature)
        self.maxpool = nn.MaxPool2d((1, 2))

    def forward(self, x, output):
        x = self.phi_bn(self.phi(x))
        psi1 = torch.mean(x, 2, keepdim=True)
        psi = psi1
        output = output + torch.mean(psi[:, :self.num_output, :, :], 3).squeeze(2)# to check, maybe a bug in pytorch
        psi = psi.expand(-1, -1, x.size(2), -1)
        x = torch.cat((x, psi), 1)
        x = F.relu(self.maxpool(x))

        return x, output


class SPIDNA2(nn.Module):
    def __init__(self, num_output, num_block, num_feature, device, **kwargs):
        super(SPIDNA2, self).__init__()
        self.num_output = num_output
        self.conv_pos =  nn.Conv2d(1, num_feature, (1, 3))
        self.conv_pos_bn = nn.BatchNorm2d(num_feature)
        self.conv_snp =  nn.Conv2d(1, num_feature, (1, 3))
        self.conv_snp_bn = nn.BatchNorm2d(num_feature)
        self.blocks = nn.ModuleList([SPIDNA2Block(num_output, num_feature) for i in range(num_block)])
        self.device = device

    def forward(self, x):
        pos = x[:, 0, :].view(x.shape[0], 1, 1, -1)
        snp = x[:, 1:, :].unsqueeze(1)
        pos = F.relu(self.conv_pos_bn(self.conv_pos(pos))).expand(-1, -1, snp.size(2), -1)
        snp = F.relu(self.conv_snp_bn(self.conv_snp(snp)))
        x = torch.cat((pos, snp), 1)
        output = torch.zeros(x.size(0), self.num_output).to(self.device)
        for block in self.blocks:
            x, output = block(x, output)


class SPIDNA2Block_loss(nn.Module):
    def __init__(self, num_output, num_feature):
        super(SPIDNA2Block_loss, self).__init__()
        self.num_output = num_output
        self.phi = nn.Conv2d(num_feature * 2, num_feature, (1, 3))
        self.phi_bn = nn.BatchNorm2d(num_feature)
        self.maxpool = nn.MaxPool2d((1, 2))

    def forward(self, x, output):
        x = self.phi_bn(self.phi(x))
        psi = torch.mean(x, 2, keepdim=True)
        output = output + torch.mean(psi[:, :self.num_output, :, :], 3).squeeze(2)# to check, maybe a bug in pytorch
        psi = psi.expand(-1, -1, x.size(2), -1)
        x = torch.cat((x, psi), 1)
        x = F.relu(self.maxpool(x))

        return x, output


class SPIDNA2_loss(nn.Module):
    def __init__(self, num_output, num_block, num_feature, device, **kwargs):
        super(SPIDNA2_loss, self).__init__()
        self.num_output = num_output
        self.conv_pos =  nn.Conv2d(1, num_feature, (1, 3))
        self.conv_pos_bn = nn.BatchNorm2d(num_feature)
        self.conv_snp =  nn.Conv2d(1, num_feature, (1, 3))
        self.conv_snp_bn = nn.BatchNorm2d(num_feature)
        self.blocks = nn.ModuleList([SPIDNA2Block_loss(num_output, num_feature) for i in range(num_block)])
        self.device = device

    def forward(self, x):
        pos = x[:, 0, :].view(x.shape[0], 1, 1, -1)
        snp = x[:, 1:, :].unsqueeze(1)
        pos = F.relu(self.conv_pos_bn(self.conv_pos(pos))).expand(-1, -1, snp.size(2), -1)
        snp = F.relu(self.conv_snp_bn(self.conv_snp(snp)))
        x = torch.cat((pos, snp), 1)
        output = torch.zeros(x.size(0), self.num_output).to(self.device)
        output_list = []
        for block in self.blocks:
            x, output = block(x, output)
            output_list.append(output)
        output_list = torch.stack(output_list)

        return output_list


class SPIDNA2Block_no_bn(nn.Module):
    def __init__(self, num_output, num_feature):
        super(SPIDNA2Block_no_bn, self).__init__()
        self.num_output = num_output
        self.phi = nn.Conv2d(num_feature * 2, num_feature, (1, 3))
        self.maxpool = nn.MaxPool2d((1, 2))

    def forward(self, x, output):
        x = self.phi(x)
        psi1 = torch.mean(x, 2, keepdim=True)
        psi = psi1
        output = output + torch.mean(psi[:, :self.num_output, :, :], 3).squeeze(2)
        psi = psi.expand(-1, -1, x.size(2), -1)
        x = torch.cat((x, psi), 1)
        x = F.relu(self.maxpool(x))

        return x, output


class SPIDNA2_no_bn(nn.Module):
    def __init__(self, num_output, num_block, num_feature, device, **kwargs):
        super(SPIDNA2_no_bn, self).__init__()
        self.num_output = num_output
        self.conv_pos =  nn.Conv2d(1, num_feature, (1, 3))
        self.conv_snp =  nn.Conv2d(1, num_feature, (1, 3))
        self.blocks = nn.ModuleList([SPIDNA2Block_no_bn(num_output, num_feature) for i in range(num_block)])
        self.device = device

    def forward(self, x):
        pos = x[:, 0, :].view(x.shape[0], 1, 1, -1)
        snp = x[:, 1:, :].unsqueeze(1)
        pos = F.relu(self.conv_pos(pos)).expand(-1, -1, snp.size(2), -1)
        snp = F.relu(self.conv_snp(snp))
        x = torch.cat((pos, snp), 1)
        output = torch.zeros(x.size(0), self.num_output).to(self.device)
        for block in self.blocks:
            x, output = block(x, output)

        return output



class MLP(nn.Module):
    def __init__(self, num_SNP, num_output, num_sample):
        super(MLP, self).__init__()
        self.fc1 = nn.Linear(num_SNP*(num_sample+1), 20)
        self.batch_norm1 = nn.BatchNorm1d(20)
        self.fc2 = nn.Linear(20, 20)
        self.batch_norm2 = nn.BatchNorm1d(20)
        self.fc3 = nn.Linear(20, 10)
        self.batch_norm3 = nn.BatchNorm1d(10)
        self.fc4 = nn.Linear(10, num_output)

    def forward(self, x):
        x = x.contiguous()
        x = x.view(x.size(0), -1)
        x = F.relu(self.batch_norm1(self.fc1(x)))
        x = F.relu(self.batch_norm2(self.fc2(x)))
        x = F.relu(self.batch_norm3(self.fc3(x)))
        x = self.fc4(x)

        return x


class SPIDNA2Block_in(nn.Module):
    def __init__(self, num_output, num_feature):
        super(SPIDNA2Block_in, self).__init__()
        self.num_output = num_output
        self.phi = nn.Conv2d(num_feature * 2, num_feature, (1, 3))
        self.phi_in =  nn.GroupNorm(2, num_feature * 2)
        self.maxpool = nn.MaxPool2d((1, 2))

    def forward(self, x, output):
        x = self.phi(self.phi_in(x)) #
        psi1 = torch.mean(x, 2, keepdim=True)
        psi = psi1
        output = output + torch.mean(psi[:, :self.num_output, :, :], 3).squeeze(2)# to check, maybe a bug in pytorch
        psi = psi.expand(-1, -1, x.size(2), -1)
        x = torch.cat((x, psi), 1)
        x = F.relu(self.maxpool(x))

        return x, output


class SPIDNA2_in(nn.Module):
    def __init__(self, num_output, num_block, num_feature, device, **kwargs):
        super(SPIDNA2_in, self).__init__()
        self.num_output = num_output
        self.conv_pos =  nn.Conv2d(1, num_feature, (1, 3))
        self.conv_pos_in = nn.InstanceNorm2d(num_feature)
        self.conv_snp =  nn.Conv2d(1, num_feature, (1, 3))
        self.conv_snp_in = nn.InstanceNorm2d(num_feature)
        self.blocks = nn.ModuleList([SPIDNA2Block_in(num_output, num_feature) for i in range(num_block)])
        self.device = device


    def forward(self, x):
        pos = x[:, 0, :].view(x.shape[0], 1, 1, -1)
        snp = x[:, 1:, :].unsqueeze(1)
        pos = F.relu(self.conv_pos_in(self.conv_pos(pos))).expand(-1, -1, snp.size(2), -1)
        snp = F.relu(self.conv_snp_in(self.conv_snp(snp)))
        x = torch.cat((pos, snp), 1)
        output = torch.zeros(x.size(0), self.num_output).to(self.device)
        for block in self.blocks:
            x, output = block(x, output)

        return output

class SPIDNA2Block_in_alpha(nn.Module):
    def __init__(self, num_output, num_feature, alpha):
        super(SPIDNA2Block_in_alpha, self).__init__()
        self.num_output = num_output
        self.phi = nn.Conv2d(num_feature * 2, num_feature, (1, 3))
        self.phi_in =  nn.GroupNorm(2, num_feature * 2)
        self.maxpool = nn.MaxPool2d((1, 2))
        self.alpha = alpha
        self.n = num_feature // 2

    def forward(self, x, output):
        x = self.phi_in(x)
        x = torch.cat((x[:, :self.n] * self.alpha, x[:, self.n:] * (1 - self.alpha)), 1)
        x = self.phi(x)
        psi1 = torch.mean(x, 2, keepdim=True)
        psi = psi1
        output = output + torch.mean(psi[:, :self.num_output, :, :], 3).squeeze(2)# to check, maybe a bug in pytorch
        psi = psi.expand(-1, -1, x.size(2), -1)
        x = torch.cat((x, psi), 1)
        x = F.relu(self.maxpool(x))

        return x, output


class SPIDNA2_in_alpha(nn.Module):
    def __init__(self, num_output, num_block, num_feature, device, alpha, **kwargs):
        super(SPIDNA2_in_alpha, self).__init__()
        self.num_output = num_output
        self.conv_pos =  nn.Conv2d(1, num_feature, (1, 3))
        self.conv_pos_in = nn.InstanceNorm2d(num_feature)
        self.conv_snp =  nn.Conv2d(1, num_feature, (1, 3))
        self.conv_snp_in = nn.InstanceNorm2d(num_feature)
        self.blocks = nn.ModuleList([SPIDNA2Block_in_alpha(num_output, num_feature, alpha) for i in range(num_block)])
        self.device = device


    def forward(self, x):
        pos = x[:, 0, :].view(x.shape[0], 1, 1, -1)
        snp = x[:, 1:, :].unsqueeze(1)
        pos = F.relu(self.conv_pos_in(self.conv_pos(pos))).expand(-1, -1, snp.size(2), -1)
        snp = F.relu(self.conv_snp_in(self.conv_snp(snp)))
        x = torch.cat((pos, snp), 1)
        output = torch.zeros(x.size(0), self.num_output).to(self.device)
        for block in self.blocks:
            x, output = block(x, output)

        return output
    
    
class SPIDNA2Block_in_pos(nn.Module):
    def __init__(self, num_output, num_feature):
        super(SPIDNA2Block_in_pos, self).__init__()
        self.num_output = num_output
        self.phi = nn.Conv2d(num_feature * 2, num_feature, (1, 3))
        self.phi_in =  nn.GroupNorm(2, num_feature * 2)
        self.maxpool = nn.MaxPool2d((1, 2))

    def forward(self, x, output):
        x = self.phi(self.phi_in(x)) #
        psi1 = torch.mean(x, 2, keepdim=True)
        psi = psi1
        output = output + torch.mean(psi[:, :self.num_output, :, :], 3).squeeze(2)# to check, maybe a bug in pytorch
        psi = psi.expand(-1, -1, x.size(2), -1)
        x = torch.cat((x, psi), 1)
        x = F.relu(self.maxpool(x))

        return x, output


class SPIDNA2_in_pos(nn.Module):
    def __init__(self, num_output, num_block, num_feature, device, **kwargs):
        super(SPIDNA2_in_pos, self).__init__()
        self.num_output = num_output
        self.conv_pos =  nn.Conv2d(1, num_feature, (1, 3))
        self.conv_pos_in = nn.InstanceNorm2d(num_feature)
        self.conv_snp =  nn.Conv2d(1, num_feature, (1, 3))
        self.conv_snp_in = nn.InstanceNorm2d(num_feature)
        self.blocks = nn.ModuleList([SPIDNA2Block_in_pos(num_output, num_feature) for i in range(num_block)])
        self.device = device


    def forward(self, x):
        pos = x[:, 0, :].view(x.shape[0], 1, 1, -1)
        snp = x[:, 1:, :].unsqueeze(1)
        pos = F.relu(self.conv_pos_in(self.conv_pos(pos))).expand(-1, -1, snp.size(2), -1)
        snp = F.relu(self.conv_snp_in(self.conv_snp(snp)))
        x = torch.cat((pos, snp), 1)
        output = torch.zeros(x.size(0), self.num_output).to(self.device)
        for block in self.blocks:
            x, output = block(x, output)

        return output
    
    
class SPIDNA2Block_in_fc(nn.Module):
    def __init__(self, num_output, num_feature):
        super(SPIDNA2Block_in_fc, self).__init__()
        self.num_output = num_output
        self.phi = nn.Conv2d(num_feature * 2, num_feature, (1, 3))
        self.phi_in =  nn.GroupNorm(2, num_feature * 2)
        self.maxpool = nn.MaxPool2d((1, 2))
        self.fc = nn.Linear(num_output, num_output)

    def forward(self, x, output):
        x = self.phi(self.phi_in(x))
        psi1 = torch.mean(x, 2, keepdim=True)
        psi = psi1
        current_output = self.fc(torch.mean(psi[:, :self.num_output, :, :], 3).squeeze(2))
        output = output + current_output
        psi = psi.expand(-1, -1, x.size(2), -1)
        x = torch.cat((x, psi), 1)
        x = F.relu(self.maxpool(x))
        
        return x, output


class SPIDNA2_in_fc(nn.Module):
    def __init__(self, num_output, num_block, num_feature, device, **kwargs):
        super(SPIDNA2_in_fc, self).__init__()
        self.num_output = num_output
        self.conv_pos =  nn.Conv2d(1, num_feature, (1, 3))
        self.conv_pos_in = nn.InstanceNorm2d(num_feature)
        self.conv_snp =  nn.Conv2d(1, num_feature, (1, 3))
        self.conv_snp_in = nn.InstanceNorm2d(num_feature)
        self.blocks = nn.ModuleList([SPIDNA2Block_in_fc(num_output, num_feature) for i in range(num_block)])
        self.device = device


    def forward(self, x):
        pos = x[:, 0, :].view(x.shape[0], 1, 1, -1)
        snp = x[:, 1:, :].unsqueeze(1)
        pos = F.relu(self.conv_pos_in(self.conv_pos(pos))).expand(-1, -1, snp.size(2), -1)
        snp = F.relu(self.conv_snp_in(self.conv_snp(snp)))
        x = torch.cat((pos, snp), 1)
        output = torch.zeros(x.size(0), self.num_output).to(self.device)
        for block in self.blocks:
            x, output = block(x, output)

        return output
    
    
class SPIDNA2Block_fc(nn.Module):
    def __init__(self, num_output, num_feature):
        super(SPIDNA2Block_fc, self).__init__()
        self.num_output = num_output
        self.phi = nn.Conv2d(num_feature * 2, num_feature, (1, 3))
        self.phi_bn = nn.BatchNorm2d(num_feature * 2)
        self.maxpool = nn.MaxPool2d((1, 2))
        self.fc = nn.Linear(num_output, num_output)

    def forward(self, x, output):
        x = self.phi(self.phi_bn(x))
        psi1 = torch.mean(x, 2, keepdim=True)
        psi = psi1
        current_output = self.fc(torch.mean(psi[:, :self.num_output, :, :], 3).squeeze(2))
        output = output + current_output
        psi = psi.expand(-1, -1, x.size(2), -1)
        x = torch.cat((x, psi), 1)
        x = F.relu(self.maxpool(x))

        return x, output


class SPIDNA2_fc(nn.Module):
    def __init__(self, num_output, num_block, num_feature, device, **kwargs):
        super(SPIDNA2_fc, self).__init__()
        self.num_output = num_output
        self.conv_pos =  nn.Conv2d(1, num_feature, (1, 3))
        self.conv_pos_bn = nn.BatchNorm2d(num_feature)
        self.conv_snp =  nn.Conv2d(1, num_feature, (1, 3))
        self.conv_snp_bn = nn.BatchNorm2d(num_feature)
        self.blocks = nn.ModuleList([SPIDNA2Block_fc(num_output, num_feature) for i in range(num_block)])
        self.device = device

    def forward(self, x):
        pos = x[:, 0, :].view(x.shape[0], 1, 1, -1)
        snp = x[:, 1:, :].unsqueeze(1)
        pos = F.relu(self.conv_pos_bn(self.conv_pos(pos))).expand(-1, -1, snp.size(2), -1)
        snp = F.relu(self.conv_snp_bn(self.conv_snp(snp)))
        x = torch.cat((pos, snp), 1)
        output = torch.zeros(x.size(0), self.num_output).to(self.device)
        for block in self.blocks:
            x, output = block(x, output)

        return output
    
    
class SPIDNA2Block_in_fc_alpha(nn.Module):
    def __init__(self, num_output, num_feature, alpha):
        super(SPIDNA2Block_in_fc_alpha, self).__init__()
        self.num_output = num_output
        self.phi = nn.Conv2d(num_feature * 2, num_feature, (1, 3))
        self.phi_in =  nn.GroupNorm(2, num_feature * 2)
        self.maxpool = nn.MaxPool2d((1, 2))
        self.fc = nn.Linear(num_output, num_output)
        self.alpha = alpha
        self.alpha1 = np.sqrt(self.alpha)
        self.alpha2 = np.sqrt(1 - self.alpha)
        self.n = num_feature // 2
        

    def forward(self, x, output):
        x = self.phi_in(x)
        x = torch.cat((x[:, :self.n] * self.alpha1, x[:, self.n:] * self.alpha2), 1)
        x = self.phi(x)
        psi = torch.mean(x, 2, keepdim=True)
        current_output = self.fc(torch.mean(psi[:, :self.num_output, :, :], 3).squeeze(2))
        output = output + current_output
        psi = psi.expand(-1, -1, x.size(2), -1)
        x = torch.cat((x, psi), 1)
        x = F.relu(self.maxpool(x))
        
        return x, output


class SPIDNA2_in_fc_alpha(nn.Module):
    def __init__(self, num_output, num_block, num_feature, device, alpha, **kwargs):
        super(SPIDNA2_in_fc_alpha, self).__init__()
        self.num_output = num_output
        self.conv_pos =  nn.Conv2d(1, num_feature, (1, 3))
        self.conv_pos_in = nn.InstanceNorm2d(num_feature)
        self.conv_snp =  nn.Conv2d(1, num_feature, (1, 3))
        self.conv_snp_in = nn.InstanceNorm2d(num_feature)
        self.blocks = nn.ModuleList([SPIDNA2Block_in_fc_alpha(num_output, num_feature, alpha) for i in range(num_block)])
        self.device = device


    def forward(self, x):
        pos = x[:, 0, :].view(x.shape[0], 1, 1, -1)
        snp = x[:, 1:, :].unsqueeze(1)
        pos = F.relu(self.conv_pos_in(self.conv_pos(pos))).expand(-1, -1, snp.size(2), -1)
        snp = F.relu(self.conv_snp_in(self.conv_snp(snp)))
        x = torch.cat((pos, snp), 1)
        output = torch.zeros(x.size(0), self.num_output).to(self.device)
        for block in self.blocks:
            x, output = block(x, output)

        return output

class SPIDNA2Block_in_all_fc(nn.Module):
    def __init__(self, num_output, num_feature):
        super(SPIDNA2Block_in_all_fc, self).__init__()
        self.num_output = num_output
        self.phi = nn.Conv2d(num_feature * 2, num_feature, (1, 3))
        self.phi_in =  nn.GroupNorm(1, num_feature * 2)
        self.maxpool = nn.MaxPool2d((1, 2))
        self.fc = nn.Linear(num_output, num_output)
        

    def forward(self, x, output):
        x = self.phi_in(x)
        x = self.phi(x)
        psi = torch.mean(x, 2, keepdim=True)
        current_output = self.fc(torch.mean(psi[:, :self.num_output, :, :], 3).squeeze(2))
        output = output + current_output
        psi = psi.expand(-1, -1, x.size(2), -1)
        x = torch.cat((x, psi), 1)
        x = F.relu(self.maxpool(x))
        
        return x, output


class SPIDNA2_in_all_fc(nn.Module):
    def __init__(self, num_output, num_block, num_feature, device, **kwargs):
        super(SPIDNA2_in_all_fc, self).__init__()
        self.num_output = num_output
        self.conv_pos =  nn.Conv2d(1, num_feature, (1, 3))
        self.conv_pos_in = nn.InstanceNorm2d(num_feature)
        self.conv_snp =  nn.Conv2d(1, num_feature, (1, 3))
        self.conv_snp_in = nn.InstanceNorm2d(num_feature)
        self.blocks = nn.ModuleList([SPIDNA2Block_in_all_fc(num_output, num_feature) for i in range(num_block)])
        self.device = device


    def forward(self, x):
        pos = x[:, 0, :].view(x.shape[0], 1, 1, -1)
        snp = x[:, 1:, :].unsqueeze(1)
        pos = F.relu(self.conv_pos_in(self.conv_pos(pos))).expand(-1, -1, snp.size(2), -1)
        snp = F.relu(self.conv_snp_in(self.conv_snp(snp)))
        x = torch.cat((pos, snp), 1)
        output = torch.zeros(x.size(0), self.num_output).to(self.device)
        for block in self.blocks:
            x, output = block(x, output)

        return output
    
class SPIDNA2Block_bn_last_layer(nn.Module):
    def __init__(self, num_output, num_feature):
        super(SPIDNA2Block_bn_last_layer, self).__init__()
        self.num_output = num_output
        self.phi = nn.Conv2d(num_feature * 2, num_feature, (1, 3))
        self.phi_bn = nn.BatchNorm2d(num_feature * 2)
        self.maxpool = nn.MaxPool2d((1, 2))

    def forward(self, x):
        x = self.phi(self.phi_bn(x))
        psi1 = torch.mean(x, 2, keepdim=True)
        psi = psi1
        psi = psi.expand(-1, -1, x.size(2), -1)
        x = torch.cat((x, psi), 1)
        x = F.relu(self.maxpool(x))

        return x
    
class SPIDNA2Block_bn_last_layer_last(nn.Module):
    def __init__(self, num_output, num_feature):
        super(SPIDNA2Block_bn_last_layer_last, self).__init__()
        self.num_output = num_output
        self.phi = nn.Conv2d(num_feature * 2, num_feature, (1, 3))
        self.phi_bn = nn.BatchNorm2d(num_feature * 2)
        self.maxpool = nn.MaxPool2d((1, 2))
        self.fc = nn.Linear(num_output, num_output)

    def forward(self, x):
        x = self.phi(self.phi_bn(x))
        psi1 = torch.mean(x, 2, keepdim=True)
        psi = psi1
        current_output = self.fc(torch.mean(psi[:, :self.num_output, :, :], 3).squeeze(2))
        output = current_output

        return output


class SPIDNA2_bn_last_layer(nn.Module):
    def __init__(self, num_output, num_block, num_feature, device, **kwargs):
        super(SPIDNA2_bn_last_layer, self).__init__()
        self.num_block = num_block
        self.num_output = num_output
        self.conv_pos =  nn.Conv2d(1, num_feature, (1, 3))
        self.conv_pos_bn = nn.BatchNorm2d(num_feature)
        self.conv_snp =  nn.Conv2d(1, num_feature, (1, 3))
        self.conv_snp_bn = nn.BatchNorm2d(num_feature)
        #self.blocks = nn.ModuleList([SPIDNA2Block_bn_last_layer(num_output, num_feature) for i in range(self.num_block-1)].append(SPIDNA2Block_bn_last_layer_last(num_output, num_feature)))
        self.blocks = nn.ModuleList([SPIDNA2Block_bn_last_layer(num_output, num_feature) for i in range(self.num_block-1)])
        self.last = SPIDNA2Block_bn_last_layer_last(num_output, num_feature)
        self.device = device
                                 
    def forward(self, x):
        pos = x[:, 0, :].view(x.shape[0], 1, 1, -1)
        snp = x[:, 1:, :].unsqueeze(1)
        pos = F.relu(self.conv_pos_bn(self.conv_pos(pos))).expand(-1, -1, snp.size(2), -1)
        snp = F.relu(self.conv_snp_bn(self.conv_snp(snp)))
        x = torch.cat((pos, snp), 1)
        output = torch.zeros(x.size(0), self.num_output).to(self.device)
        for block in self.blocks[:self.num_block-1]:
            x = block(x)
        output = self.last(x)

        return output
