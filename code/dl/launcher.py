import argparse
import pickle
import data_preprocessing
import training
import numpy as np
import sys
import os
import logging

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--loglevel',
                        default='INFO',
                        action='store',
                        type=str,
                        metavar='INFO',
                        help='Amount of log info: DEBUG, [INFO], WARNING, ERROR, CRITICAL')
    args = parser.parse_args()
    logging.basicConfig(stream=sys.stdout,
                        #filename=os.path.join(out_dir, 'output.log'),
                        level=logging.getLevelName(args.loglevel),
                        format='%(asctime)s; %(levelname)s;  %(message)s',
                        datefmt='%d/%m/%Y %H:%M:%S')
    #training_param = data_preprocessing.TrainingParam()
    training_param_path = '/home/tau/thsanche/run_long/cattle/special_best_SPIDNA2_fc_400snp/cattle_special_SPIDNA2_fc_400snp_training_param.json'
    #training_param_path = training_param.preprocess(overwrite=False)
    training.start_training(training_param_path, None)
