#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from collections import OrderedDict
import numpy as np
import net as net_module
import pandas as pd
import torch
    
def infer(data, network_path, network_name, SNP_min, num_block, num_feature, train_mean, train_std, param_2_learn, sample_min, **kwargs):

    with torch.no_grad():
        num_output = len(param_2_learn)
        device = torch.device('cuda' if torch.cuda.is_available else 'cpu')
        data = data.to(device)
        net = getattr(net_module, network_name)
        if network_name == 'SPIDNA2' or network_name == 'SPIDNA2_no_bn' or network_name == 'SPIDNA2_loss':
            net_param = {'num_output': num_output,
                         'num_block': num_block,
                         'num_feature': num_feature,
                         'device':device}
        else:
            net_param = {'num_SNP': SNP_min,
                         'num_sample': sample_min,
                         'num_output': num_output}
        net = net(**net_param).to(device)
        state_dict = torch.load(network_path)
        # create new OrderedDict that does not contain `module.`
        new_state_dict = OrderedDict()
        for k, v in state_dict.items():
            name = k[7:] # remove `module.`
            new_state_dict[name] = v
        net.load_state_dict(new_state_dict)
        net.eval() 

        output = net(data).detach().cpu().numpy()[0]
    #return  np.exp((output + list(train_mean.values())) * list(train_std.values()))
    return  output