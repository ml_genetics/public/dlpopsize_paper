import logging
import sys
sys.path.append('/home/tau/thsanche/dnadna/dl')
import argparse
import pickle
import time
import hpbandster.core.nameserver as hpns
import hpbandster.core.result as hpres
from hpbandster.optimizers import BOHB as BOHB
import ConfigSpace as CS
import ConfigSpace.hyperparameters as CSH
from hpbandster.core.worker import Worker
import training
import numpy as np
import sys
import os
from shutil import copyfile
import utils
import time
import data_preprocessing

class MyWorker(Worker):

    def __init__(self, *args, sleep_interval=0, **kwargs):
        super().__init__(*args, **kwargs)
        self.sleep_interval = sleep_interval

    def compute(self, config, budget, **kwargs):
        current_run_path = '/home/tau/thsanche/run_400snp/cattle/'
        training_param_path = os.path.join(current_run_path, 'run_0/cattle_run_0_training_param.json')
        data_preprocessed_path = os.path.join(current_run_path ,'run_0/cattle_run_0_preprocessed_param.csv')
        training_param = data_preprocessing.TrainingParam(training_param_path=training_param_path)
        training_param.learning_rate = config['learning_rate']
        training_param.batch_size = config['batch_size']
        training_param.network_name = config['network_name']

        '''if config['network_name'] in ['SPIDNA2_no_bn', 'SPIDNA2_in', 'SPIDNA2_in_pos', 'SPIDNA2_in_fc', 'SPIDNA2_in_alpha']:
            if config['snp_min'] is 'None':
                training_param.snp_min = None
            else:
                training_param.snp_min = config['snp_min']'''
   
        training_param.SNP_min = 400 ##
        #training_param.SNP_min = None ##
        training_param.weight_decay = config['weight_decay']
        if config['network_name']=='SPIDNA2_in_fc_alpha':
            training_param.alpha = np.log(config['alpha'])
        logging.info(vars(training_param))
        simulation_param = utils.load_dict_from_json(training_param.simulation_param_path)
        np.random.seed(training_param.seed)
        run_dir_list = [i for i in os.listdir(os.path.join(training_param.run_path, simulation_param['model_name'])) if i[:3] == 'run']
        num_run = int(training_param.run_name.split('_')[-1])
        num_run = str(max([int(dir_name.split('_')[-1]) for dir_name in run_dir_list] + [num_run]) + 1)
        training_param.run_name = f'run_{num_run}'
        logging.info(f'run_{num_run}')
        os.makedirs(os.path.join(current_run_path, training_param.run_name), exist_ok=True)
        
        new_data_preprocessed_path = os.path.join(current_run_path, training_param.run_name, f'{simulation_param["model_name"]}_{training_param.run_name}_preprocessed_param.csv')
        training_param.preprocessed_scenario_param_path = new_data_preprocessed_path
        copyfile(data_preprocessed_path, new_data_preprocessed_path)
        new_training_param_path  = os.path.join(current_run_path, training_param.run_name, f'{simulation_param["model_name"]}_{training_param.run_name}_training_param.json')

        training_param.save(new_training_param_path)
        training_param_path = new_training_param_path
        res = np.mean(training.start_training(training_param_path, budget))
        time.sleep(self.sleep_interval)
        res = float(res)
        return({
                    'loss': res,  # this is the a mandatory field to run hyperband
                    'info': training_param.run_name  # can be used for any user-defined information - also mandatory
                })

    @staticmethod
    def get_configspace():
        config_space = CS.ConfigurationSpace()
        learning_rate = CS.UniformFloatHyperparameter('learning_rate', lower=0.00005, upper=0.01)
        batch_size = CS.UniformIntegerHyperparameter('batch_size', lower=10, upper=100)
        #network_name = CSH.CategoricalHyperparameter('network_name', ['SPIDNA1', 'SPIDNA2', 'SPIDNA2_no_bn', 'MLP', 'SPIDNA2_in', 'SPIDNA2_in_pos', 'SPIDNA2_in_fc', 'SPIDNA2_in_alpha'])
        network_name = CSH.CategoricalHyperparameter('network_name', ['SPIDNA1', 'SPIDNA2_fc', 'MLP', 'SPIDNA2_in_fc_alpha', 'SPIDNA2_in_all_fc'])##
        #network_name = CSH.CategoricalHyperparameter('network_name', ['SPIDNA2_in_all_fc', 'SPIDNA2_in_fc_alpha'])##

        #snp_min = CSH.CategoricalHyperparameter('snp_min', [400, 'None'])
        weight_decay = CS.UniformFloatHyperparameter('weight_decay', lower=0.00005, upper=0.01)
        alpha = CS.UniformFloatHyperparameter('alpha', lower=np.exp(0.5), upper=np.exp(1))
        #config_space.add_hyperparameters([learning_rate, batch_size, network_name, snp_min, weight_decay, alpha])
        config_space.add_hyperparameters([learning_rate, batch_size, network_name, weight_decay, alpha])
        config_space.add_condition(CS.EqualsCondition(alpha, network_name, 'SPIDNA2_in_fc_alpha'))
        #config_space.add_condition(CS.InCondition(snp_min, network_name, ['SPIDNA2_no_bn', 'SPIDNA2_in', 'SPIDNA2_in_pos', 'SPIDNA2_in_fc', 'SPIDNA2_in_alpha']))      
                             
        return(config_space)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--n_workers', type=int)
    parser.add_argument('--nic_name', type=str, help='Which network interface to use for communication.')
    parser.add_argument('--port', type=int, help='Port')
    parser.add_argument('--working_dir',type=str, help='A directory that is accessible for all processes, e.g. a NFS share.')
    parser.add_argument('--worker', help='Flag to turn this into a worker process', action='store_true')
    parser.add_argument('--min_budget',   type=float, help='Minimum budget used during the optimization.',    default=1e6)
    parser.add_argument('--max_budget',   type=float, help='Maximum budget used during the optimization.',    default=1e7)
    parser.add_argument('--eta', type=int,   help='Eta', default=3)
    parser.add_argument('--n_iterations', type=int,   help='Number of iterations performed by the optimizer', default=10)
    parser.add_argument('--run_id', action='store', type=str)
    parser.add_argument('--loglevel', default='INFO', action='store', type=str, metavar='INFO', help='Amount of log info: DEBUG, [INFO], WARNING, ERROR, CRITICAL')
    args = parser.parse_args()
    logging.basicConfig(stream=sys.stdout, level=logging.getLevelName(args.loglevel), format='%(asctime)s; %(levelname)s;  %(message)s', datefmt='%d/%m/%Y %H:%M:%S')
    host = hpns.nic_name_to_host(args.nic_name)
    if args.worker:
        time.sleep(5)   # short artificial delay to make sure the nameserver is already running
        w = MyWorker(sleep_interval = 0.5, run_id=args.run_id, host=host, id=os.environ['SLURM_ARRAY_TASK_ID'])
        w.load_nameserver_credentials(working_directory=args.working_dir)
        w.run(background=False)
        exit(0)
 
    '''max_SH_iter = -int(np.log(args.min_budget/args.max_budget)/np.log(args.eta)) + 1
    budgets = args.max_budget * np.power(args.eta, -np.linspace(max_SH_iter-1, 0, max_SH_iter))
    n = 0
    for i in range(max_SH_iter):
        s = max_SH_iter - 1 - i
        n += int(np.floor((max_SH_iter)/(s+1)) * args.eta**s)
    logging.info(f'{n} unique configurations will be sampled.')'''

    result_logger = hpres.json_result_logger(directory=args.working_dir, overwrite=False)

    NS = hpns.NameServer(run_id=args.run_id, host=host, port=args.port, working_directory=args.working_dir)
    ns_host, ns_port = NS.start()
    
    w = MyWorker(sleep_interval = 0.5, run_id=args.run_id, host=host, nameserver=ns_host, nameserver_port=ns_port)
    w.run(background=True)
    
    bohb = BOHB(configspace = MyWorker.get_configspace(),
                eta = args.eta,
                run_id = args.run_id,
                host=host,
                nameserver=ns_host,
                nameserver_port=ns_port,
                result_logger=result_logger,
                min_budget=args.min_budget, max_budget=args.max_budget)

    res = bohb.run(n_iterations=args.n_iterations, min_n_workers=args.n_workers)
    with open(os.path.join(args.working_dir, f'{args.run_id}_results.pkl'), 'wb') as fh:
        pickle.dump(res, fh)

    bohb.shutdown(shutdown_workers=True)
    NS.shutdown()
    id2config = res.get_id2config_mapping()
    incumbent = res.get_incumbent_id()
    logging.info(f'Best found configuration: {id2config[incumbent]["config"]}')
    logging.info(f'A total of {len(id2config.keys())} unique configurations where sampled.')
    logging.info(f'A total of {len(res.get_all_runs())} runs where executed.')
    logging.info(f'Total budget corresponds to {(sum([r.budget for r in res.get_all_runs()])/args.max_budget)} full function evaluations.')
