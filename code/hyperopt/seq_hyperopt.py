import logging

import argparse
import pickle
import time
import hpbandster.core.nameserver as hpns
import hpbandster.core.result as hpres
from hpbandster.optimizers import BOHB as BOHB
import ConfigSpace as CS
import ConfigSpace.hyperparameters as CSH
from hpbandster.core.worker import Worker
import data_preprocessing
import training
import numpy as np
import sys
import os
from shutil import copyfile
import utils

class MyWorker(Worker):

    def __init__(self, *args, sleep_interval=0, **kwargs):
        super().__init__(*args, **kwargs)
        self.sleep_interval = sleep_interval

    def compute(self, config, budget, **kwargs):
        logging.info(config)
        #training_param = data_preprocessing.TrainingParam(learning_rate=config['learning_rate'],
        #                                                  batch_size=config['batch_size'],
        #                                                  SNP_min=config['SNP_min'],
        #                                                  num_block=config['num_block'])
        #training_param_path = training_param.preprocess(overwrite=False)
        current_run_path = '/home/tau/thsanche/run/cattle/'
        training_param_path = os.path.join(current_run_path, 'run_0/cattle_run_0_training_param.json')
        data_preprocessed_path = os.path.join(current_run_path ,'run_0/cattle_run_0_preprocessed_param.csv')
        training_param = data_preprocessing.TrainingParam(training_param_path=training_param_path)
        training_param.learning_rate = config['learning_rate']
        training_param.batch_size = config['batch_size']
        training_param.network_name = config['network_name']
        if config['network_name'] in ['SPIDNA2_no_bn', 'SPIDNA2_in', 'SPIDNA2_in_pos', 'SPIDNA2_in_fc', 'SPIDNA2_in_alpha']:
            if config['snp_min'] is 'None':
                training_param.snp_min = None
            else:
                training_param.snp_min = config['snp_min']
        training_param.weight_decay = config['weight_decay']
        if config['network_name'] is 'SPIDNA2_in_alpha':
            training_param.alpha = config['alpha']
        #training_param.SNP_min = config['SNP_min']
        #training_param.num_block = config['num_block']
        logging.debug(vars(training_param))
        simulation_param = utils.load_dict_from_json(training_param.simulation_param_path)
        np.random.seed(training_param.seed)
        run_dir_list = [i for i in os.listdir(os.path.join(training_param.run_path, simulation_param['model_name'])) if i[:3] == 'run']
        num_run = int(training_param.run_name.split('_')[-1])
        num_run = str(max([int(dir_name.split('_')[-1]) for dir_name in run_dir_list] + [num_run]) + 1)
        training_param.run_name = f'run_{num_run}'
        os.makedirs(os.path.join(current_run_path, training_param.run_name), exist_ok=True)
        new_data_preprocessed_path = os.path.join(current_run_path, training_param.run_name, f'{simulation_param["model_name"]}_{training_param.run_name}_preprocessed_param.csv')
        training_param.preprocessed_scenario_param_path = new_data_preprocessed_path
        copyfile(data_preprocessed_path, new_data_preprocessed_path)
        new_training_param_path  = os.path.join(current_run_path, training_param.run_name, f'{simulation_param["model_name"]}_{training_param.run_name}_training_param.json')
        training_param.save(new_training_param_path)
        training_param_path = new_training_param_path
        
        res = np.mean(training.start_training(training_param_path, budget))
        time.sleep(self.sleep_interval)
        res = float(res)
        return({
                    'loss': res,  # this is the a mandatory field to run hyperband
                    'info': res  # can be used for any user-defined information - also mandatory
                })

    @staticmethod
    def get_configspace():
        config_space = CS.ConfigurationSpace()
        learning_rate = CS.UniformFloatHyperparameter('learning_rate', lower=0.0001, upper=0.01)
        batch_size = CS.UniformIntegerHyperparameter('batch_size', lower=50, upper=200)
        network_name = CSH.CategoricalHyperparameter('network_name', ['SPIDNA1', 'SPIDNA2', 'SPIDNA2_no_bn', 'MLP', 'SPIDNA2_in', 'SPIDNA2_in_pos', 'SPIDNA2_in_fc', 'SPIDNA2_in_alpha'])
        snp_min = CSH.CategoricalHyperparameter('snp_min', [400, 'None'])
        weight_decay = CS.UniformFloatHyperparameter('weight_decay', lower=0.0001, upper=0.01)
        alpha = CS.UniformFloatHyperparameter('alpha', lower=0.4, upper=0.9)
        config_space.add_hyperparameters([learning_rate, batch_size, network_name, snp_min, weight_decay, alpha])
        config_space.add_condition(CS.EqualsCondition(alpha, network_name, 'SPIDNA2_in_alpha'))
        config_space.add_condition(CS.InCondition(snp_min, network_name, ['SPIDNA2_no_bn', 'SPIDNA2_in', 'SPIDNA2_in_pos', 'SPIDNA2_in_fc', 'SPIDNA2_in_alpha']))      
                             
        return(config_space)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--min_budget',   type=float, help='Minimum budget used during the optimization.',    default=1e6)
    parser.add_argument('--max_budget',   type=float, help='Maximum budget used during the optimization.',    default=1e7)
    parser.add_argument('--n_iterations', type=int,   help='Number of iterations performed by the optimizer', default=5)
    parser.add_argument('--run_id',
                        action='store',
                        type=str)
    parser.add_argument('--loglevel',
                        default='INFO',
                        action='store',
                        type=str,
                        metavar='INFO',
                        help='Amount of log info: DEBUG, [INFO], WARNING, ERROR, CRITICAL')
    args = parser.parse_args()
    logging.basicConfig(stream=sys.stdout,
                        #filename=os.path.join(out_dir, 'output.log'),
                        level=logging.getLevelName(args.loglevel),
                        format='%(asctime)s; %(levelname)s;  %(message)s',
                        datefmt='%d/%m/%Y %H:%M:%S')
    NS = hpns.NameServer(run_id=args.run_id, host='127.0.0.1', port=None)
    NS.start()                                        
    w = MyWorker(sleep_interval=0, nameserver='127.0.0.1', run_id=args.run_id)
    w.run(background=True)
                                                
                                                
    '''bohb = BOHB(configspace=w.get_configspace(), run_id=args.run_id, nameserver='127.0.0.1', min_budget=args.min_budget, max_budget=args.max_budget)
    res = bohb.run(n_iterations=args.n_iterations)
    with open(os.path.join(f'{args.run_id}_results.pkl'), 'wb') as fh:
                              pickle.dump(res, fh)
    bohb.shutdown(shutdown_workers=True)
    NS.shutdown()
    id2config = res.get_id2config_mapping()
    incumbent = res.get_incumbent_id()

    logging.info(f'Best found configuration: {id2config[incumbent]["config"]}')
    logging.info(f'A total of {len(id2config.keys())} unique configurations where sampled.')
    logging.info(f'A total of {len(res.get_all_runs())} runs where executed.')
    logging.info(f'Total budget corresponds to {(sum([r.budget for r in res.get_all_runs()])/args.max_budget)} full function evaluations.') '''                                           
                                                
                                                
    eta = 3
    max_SH_iter = -int(np.log(args.min_budget/args.max_budget)/np.log(eta)) + 1
    budgets = args.max_budget * np.power(eta, -np.linspace(max_SH_iter-1, 0, max_SH_iter))
    n = 0
    for i in range(max_SH_iter):
        s = max_SH_iter - 1 - i
        n += int(np.floor((max_SH_iter)/(s+1)) * eta**s)
        logging.info(f'{n} unique configurations will be sampled.')
        logging.info(f'Budgets list: {budgets}')
        bohb = BOHB(configspace = w.get_configspace(),
                    run_id = args.run_id, nameserver='127.0.0.1',
                    min_budget=args.min_budget, max_budget=args.max_budget)
        res = bohb.run(n_iterations=args.n_iterations)
        with open(os.path.join(f'{args.run_id}_results.pkl'), 'wb') as fh:
            pickle.dump(res, fh)
        bohb.shutdown(shutdown_workers=True)
        NS.shutdown()
        id2config = res.get_id2config_mapping()
        incumbent = res.get_incumbent_id()

        logging.info(f'Best found configuration: {id2config[incumbent]["config"]}')
        logging.info(f'A total of {len(id2config.keys())} unique configurations where sampled.')
        logging.info(f'A total of {len(res.get_all_runs())} runs where executed.')
        logging.info(f'Total budget corresponds to {(sum([r.budget for r in res.get_all_runs()])/args.max_budget)} full function evaluations.')
